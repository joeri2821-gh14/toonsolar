import QtQuick 2.1
import qb.components 1.0
import qb.base 1.0

Tile {
	id: gh14solarTile
        
    property bool dimState: screenStateController.dimmedColors
	property real valueProduced: app.production !== undefined ? app.production : NaN
	property real valueProducedToday: app.productionToday !== undefined ? app.productionToday : NaN
	property string valueText: ""
	property bool showTodayProduction: false

	QtObject {
		id: p

		property int animationIndex: 0
	}

	onClicked: {
        if (app.gh14solarDetailsScreen) {
            app.gh14solarDetailsScreen.show();
        }
    }

	onValueProducedChanged: {
		if (isNaN(valueProduced) || valueProduced === 0)
			p.animationIndex = 0;
	}
	
	function toggleValue() {
        if (showTodayProduction) {
			showTodayProduction = false;
			valueText = isNaN(valueProduced) ? "-" : qsTr("%1 Watt").arg(valueProduced);
		} else {
			showTodayProduction = true;
			valueText = isNaN(valueProducedToday) ? "-" : qsTr("%1 KWh").arg(i18n.number( Number( valueProducedToday ), 1 ));
		}
	}

	Text {
		id: tileTitle
		anchors {
			baseline: parent.top
			baselineOffset: 30
			horizontalCenter: parent.horizontalCenter
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		color: colors.tileTitleColor
		text: "Zonnepanelen nu"
                visible: !dimState
	}

	Image {
		id: panelImage
		source: "image://scaled/apps/graph/drawables/PanelsDots0" + (dimState ? "Dim" : "") + ".svg"
		anchors {
			bottom: parent.bottom
			bottomMargin: Math.round(50 * verticalScaling)
			left: parent.left
			leftMargin: Math.round(72 * horizontalScaling)
		}
	}

	Rectangle {
		id: dotBig
		height: Math.round(12 * verticalScaling)
		width: height
		radius: height / 2
		color: colors.graphSolarThisMomentDot
		anchors {
			top: dotMedium.bottom
			topMargin: Math.round(3 * verticalScaling)
			left: dotMedium.right
			leftMargin: Math.round(3 * horizontalScaling)
		}
		visible: p.animationIndex >= 3
	}

	Rectangle {
		id: dotMedium
		height: Math.round(8 * verticalScaling)
		width: height
		radius: height / 2
		color: colors.graphSolarThisMomentDot
		anchors {
			top: dotSmall.bottom
			topMargin: Math.round(3 * verticalScaling)
			left: dotSmall.right
			leftMargin: Math.round(3 * horizontalScaling)
		}
		visible: p.animationIndex >= 2 && p.animationIndex <= 4
	}

	Rectangle {
		id: dotSmall
		height: Math.round(5 * verticalScaling)
		width: height
		radius: height / 2
		color: colors.graphSolarThisMomentDot
		anchors {
			top: panelImage.top
			topMargin: 0
			left: panelImage.left
			leftMargin: 0
		}
		visible: p.animationIndex >= 1 && p.animationIndex <= 3
	}

	Text {
		id: tileValue
		anchors {
			baseline: parent.bottom
			baselineOffset: designElements.vMarginNeg16
			horizontalCenter: parent.horizontalCenter
		}
		font {
			family: qfont.regular.name
			pixelSize: 28
		}
		color: (typeof dimmableColors !== 'undefined') ? dimmableColors.clockTileColor : (typeof dimmableColors !== 'undefined') ? dimmableColors.clockTileColor : colors.clockTileColor
		text: valueText
	}

	Timer {
		id: animationTimer
		interval: 500
		repeat: true
		running: valueProduced > 0
		onTriggered: p.animationIndex = (p.animationIndex + 1) % 6
	}
	
	Timer {
		id: textTimer
		interval: 15000
		repeat: true
		running: true
		onTriggered: toggleValue()
	}
}
