import QtQuick 2.1
import qb.components 1.0
import qb.base 1.0

Screen {
	id: gh14solarDetailsScreen
    screenTitle: "Zonnepanelen opbrengst details (KW/h)"
    property string serverDetailsPath: "/toon/solardetails"
    
    // production
    property double productionToday: NaN
    property double productionYesterday: NaN
    property double productionThisWeek: NaN
    property double productionLastWeek: NaN
    property double productionThisMonth: NaN
    property double productionLastMonth: NaN
    property double productionThisYear: NaN
    property double productionLastYear: NaN
    
    // usage
    property double usageToday: NaN
    property double usageYesterday: NaN
    property double usageThisWeek: NaN
    property double usageLastWeek: NaN
    property double usageThisMonth: NaN
    property double usageLastMonth: NaN
    property double usageThisYear: NaN
    property double usageLastYear: NaN
    
    // in
    property double inToday: NaN
    property double inYesterday: NaN
    property double inThisWeek: NaN
    property double inLastWeek: NaN
    property double inThisMonth: NaN
    property double inLastMonth: NaN
    property double inThisYear: NaN
    property double inLastYear: NaN
    
    // out
    property double outToday: NaN
    property double outYesterday: NaN
    property double outThisWeek: NaN
    property double outLastWeek: NaN
    property double outThisMonth: NaN
    property double outLastMonth: NaN
    property double outThisYear: NaN
    property double outLastYear: NaN
    
    
    onShown: {
		console.log("Gh14solarApp: get details")
        var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					var json = JSON.parse( xmlhttp.responseText );
                    console.log("Gh14solarApp: processing values...")
                    
                    // production
                    productionToday = json.production.today;
                    productionYesterday = json.production.yesterday;
                    productionThisWeek = json.production.thisWeek;
                    productionLastWeek = json.production.lastWeek;
                    productionThisMonth = json.production.thisMonth;
                    productionLastMonth = json.production.lastMonth;
                    productionThisYear = json.production.thisYear;
                    productionLastYear = json.production.lastYear;
                    
                    // usage
                    usageToday = json.usage.today;
                    usageYesterday = json.usage.yesterday; 
                    usageThisWeek = json.usage.thisWeek;
                    usageLastWeek = json.usage.lastWeek;
                    usageThisMonth = json.usage.thisMonth;
                    usageLastMonth = json.usage.lastMonth;
                    usageThisYear = json.usage.thisYear;
                    usageLastYear = json.usage.lastYear;
                    
                    // in
                    inToday = json.in.today;
                    inYesterday = json.in.yesterday;
                    inThisWeek = json.in.thisWeek;
                    inLastWeek = json.in.lastWeek;
                    inThisMonth = json.in.thisMonth;
                    inLastMonth = json.in.lastMonth;
                    inThisYear = json.in.thisYear;
                    inLastYear = json.in.lastYear;
                    
                    // out
                    outToday = json.out.today;
                    outYesterday = json.out.yesterday;
                    outThisWeek = json.out.thisWeek;
                    outLastWeek = json.out.lastWeek;
                    outThisMonth = json.out.thisMonth;
                    outLastMonth = json.out.lastMonth;
                    outThisYear = json.out.thisYear;
                    outLastYear = json.out.lastYear;
				} else {
                    // Something went wrong
                    console.log("Gh14solarApp: failed to collect details, server error code " + xmlhttp.status + ", message: " + xmlhttp.statusText)
                }
            }
        }
		xmlhttp.open("GET", app.serverUrl + serverDetailsPath, true);
		xmlhttp.send();
	}
    
    StandardButton {
		id: btnInstellingen
		width: 110
		text: "Instellingen"
		anchors {
			baseline: parent.top
            baselineOffset: 360
			left: parent.left
			leftMargin: 650
		}
		onClicked: {
			if (app.gh14solarSettingsScreen) {
                console.log("Gh14solarApp: show settings screen");
                app.gh14solarSettingsScreen.show();
            } else {
                console.log("Gh14solarApp: no settings screen");
            }
		}
	}
    
    
    /* Top row (headers): horizontal 100 in between, 150 from the left */
    Text {
		id: textProduction
		anchors {
			baseline: parent.top
			baselineOffset: 20
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 150
		}
		font {
			family: qfont.semiBold.name
			pixelSize: qfont.tileTitle
		}
		text: "Opwek:"
	}
    
    Text {
		id: textUsage
		anchors {
			baseline: parent.top
			baselineOffset: 20
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 250
		}
		font {
			family: qfont.semiBold.name
			pixelSize: qfont.tileTitle
		}
		text: "Verbruik:"
	}
    
    Text {
		id: textIn
		anchors {
			baseline: parent.top
			baselineOffset: 20
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 350
		}
		font {
			family: qfont.semiBold.name
			pixelSize: qfont.tileTitle
		}
		text: "Inname:"
	}
    
    Text {
		id: textOut
		anchors {
			baseline: parent.top
			baselineOffset: 20
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 450
		}
		font {
			family: qfont.semiBold.name
			pixelSize: qfont.tileTitle
		}
		text: "Teruglevering:"
	}
       
    
    /* Left row (headers): vertical spacing in group: 30, between groups: 60 */
    Text {
		id: textToday
		anchors {
			baseline: parent.top
			baselineOffset: 60
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 20
		}
		font {
			family: qfont.semiBold.name
			pixelSize: qfont.tileTitle
		}
		text: "Vandaag:"
	}
    
    Text {
		id: textYesterday
		anchors {
			baseline: parent.top
			baselineOffset: 90
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 20
		}
		font {
			family: qfont.semiBold.name
			pixelSize: qfont.tileTitle
		}
		text: "Gisteren:"
	}
    
    Text {
		id: textThisWeek
		anchors {
			baseline: parent.top
			baselineOffset: 150
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 20
		}
		font {
			family: qfont.semiBold.name
			pixelSize: qfont.tileTitle
		}
		text: "Deze week:"
	}
    
    Text {
		id: textLastWeek
		anchors {
			baseline: parent.top
			baselineOffset: 180
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 20
		}
		font {
			family: qfont.semiBold.name
			pixelSize: qfont.tileTitle
		}
		text: "Vorige week:"
	}
    
    Text {
		id: textThisMonth
		anchors {
			baseline: parent.top
			baselineOffset: 240
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 20
		}
		font {
			family: qfont.semiBold.name
			pixelSize: qfont.tileTitle
		}
		text: "Deze maand:"
	}
    
    Text {
		id: textLastMonth
		anchors {
			baseline: parent.top
			baselineOffset: 270
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 20
		}
		font {
			family: qfont.semiBold.name
			pixelSize: qfont.tileTitle
		}
		text: "Vorige maand:"
	}
    
    Text {
		id: textThisYear
		anchors {
			baseline: parent.top
			baselineOffset: 330
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 20
		}
		font {
			family: qfont.semiBold.name
			pixelSize: qfont.tileTitle
		}
		text: "Dit jaar:"
	}
    
    Text {
		id: textLastYear
		anchors {
			baseline: parent.top
			baselineOffset: 360
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 20
		}
		font {
			family: qfont.semiBold.name
			pixelSize: qfont.tileTitle
		}
		text: "Vorig jaar:"
	}
    
    /* production values */
    Text {
		id: valueProductionToday
		anchors {
			baseline: parent.top
			baselineOffset: 60
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 150
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( productionToday ), 1 )
	}
    
    Text {
		id: valueProductionYesterday
		anchors {
			baseline: parent.top
			baselineOffset: 90
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 150
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( productionYesterday ), 1 )
	}
    
    Text {
		id: valueProductionThisWeek
		anchors {
			baseline: parent.top
			baselineOffset: 150
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 150
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( productionThisWeek ), 1 )
	}
    
    Text {
		id: valueProductionLastWeek
		anchors {
			baseline: parent.top
			baselineOffset: 180
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 150
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( productionLastWeek ), 1 )
	}
    
    Text {
		id: valueProductionThisMonth
		anchors {
			baseline: parent.top
			baselineOffset: 240
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 150
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( productionThisMonth ), 0 )
	}
    
    Text {
		id: valueProductionLastMonth
		anchors {
			baseline: parent.top
			baselineOffset: 270
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 150
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( productionLastMonth ), 0 )
	}
    
    Text {
		id: valueProductionThisYear
		anchors {
			baseline: parent.top
			baselineOffset: 330
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 150
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( productionThisYear ), 0 )
	}
    
    Text {
		id: valueProductionLastYear
		anchors {
			baseline: parent.top
			baselineOffset: 360
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 150
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( productionLastYear ), 0 )
	}
    
    /* usage values */
    Text {
		id: valueUsageToday
		anchors {
			baseline: parent.top
			baselineOffset: 60
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 250
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( usageToday ), 1 )
	}
    
    Text {
		id: valueUsageYesterday
		anchors {
			baseline: parent.top
			baselineOffset: 90
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 250
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( usageYesterday ), 1 )
	}
    
    Text {
		id: valueUsageThisWeek
		anchors {
			baseline: parent.top
			baselineOffset: 150
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 250
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( usageThisWeek ), 1 )
	}
    
    Text {
		id: valueUsageLastWeek
		anchors {
			baseline: parent.top
			baselineOffset: 180
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 250
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( usageLastWeek ), 1 )
	}
    
    Text {
		id: valueUsageThisMonth
		anchors {
			baseline: parent.top
			baselineOffset: 240
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 250
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( usageThisMonth ), 0 )
	}
    
    Text {
		id: valueUsageLastMonth
		anchors {
			baseline: parent.top
			baselineOffset: 270
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 250
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( usageLastMonth ), 0 )
	}
    
    Text {
		id: valueUsageThisYear
		anchors {
			baseline: parent.top
			baselineOffset: 330
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 250
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( usageThisYear ), 0 )
	}
    
    Text {
		id: valueUsageLastYear
		anchors {
			baseline: parent.top
			baselineOffset: 360
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 250
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( usageLastYear ), 0 )
	}
    
    /* IN values */
    Text {
		id: valueInToday
		anchors {
			baseline: parent.top
			baselineOffset: 60
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 350
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( inToday ), 1 )
	}
    
    Text {
		id: valueInYesterday
		anchors {
			baseline: parent.top
			baselineOffset: 90
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 350
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( inYesterday ), 1 )
	}
    
    Text {
		id: valueInThisWeek
		anchors {
			baseline: parent.top
			baselineOffset: 150
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 350
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( inThisWeek ), 1 )
	}
    
    Text {
		id: valueInLastWeek
		anchors {
			baseline: parent.top
			baselineOffset: 180
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 350
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( inLastWeek ), 1 )
	}
    
    Text {
		id: valueInThisMonth
		anchors {
			baseline: parent.top
			baselineOffset: 240
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 350
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( inThisMonth ), 0 )
	}
    
    Text {
		id: valueInLastMonth
		anchors {
			baseline: parent.top
			baselineOffset: 270
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 350
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( inLastMonth ), 0 )
	}
    
    Text {
		id: valueInThisYear
		anchors {
			baseline: parent.top
			baselineOffset: 330
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 350
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( inThisYear ), 0 )
	}
    
    Text {
		id: valueInLastYear
		anchors {
			baseline: parent.top
			baselineOffset: 360
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 350
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( inLastYear ), 0 )
	}
    
    /* OUT values */
    Text {
		id: valueOutToday
		anchors {
			baseline: parent.top
			baselineOffset: 60
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 450
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( outToday ), 1 )
	}
    
    Text {
		id: valueOutYesterday
		anchors {
			baseline: parent.top
			baselineOffset: 90
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 450
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( outYesterday ), 1 )
	}
    
    Text {
		id: valueOutThisWeek
		anchors {
			baseline: parent.top
			baselineOffset: 150
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 450
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( outThisWeek ), 1 )
	}
    
    Text {
		id: valueOutLastWeek
		anchors {
			baseline: parent.top
			baselineOffset: 180
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 450
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( outLastWeek ), 1 )
	}
    
    Text {
		id: valueOutThisMonth
		anchors {
			baseline: parent.top
			baselineOffset: 240
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 450
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( outThisMonth ), 0 )
	}
    
    Text {
		id: valueOutLastMonth
		anchors {
			baseline: parent.top
			baselineOffset: 270
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 450
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( outLastMonth ), 0 )
	}
    
    Text {
		id: valueOutThisYear
		anchors {
			baseline: parent.top
			baselineOffset: 330
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 450
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( outThisYear ), 0 )
	}
    
    Text {
		id: valueOutLastYear
		anchors {
			baseline: parent.top
			baselineOffset: 360
			horizontalCenter: parent.horizontalCenter
            left: parent.left
		    leftMargin: 450
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		text: i18n.number( Number( outLastYear ), 0 )
	}
    
}