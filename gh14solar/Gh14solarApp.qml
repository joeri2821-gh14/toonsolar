import QtQuick 2.1
import qb.components 1.0
import qb.base 1.0
import FileIO 1.0

/*
 * Gh14solarApp.qml
 *
 * Application that displays solar production and P1 in/out.
 */

App {
    id: gh14solarApp
    
    property Gh14solarDetailsScreen gh14solarDetailsScreen
    property Gh14solarSettingsScreen gh14solarSettingsScreen
    
    property url tileUrl : "Gh14solarTile.qml"
    property url detailScreenUrl : "Gh14solarDetailsScreen.qml"
    property url settingScreenUrl : "Gh14solarSettingsScreen.qml"
    property url thumbnailIcon: "image://apps/graph/drawables/PanelsDots0.svg"
    property string serverProductionPath: "/toon/solarpower"
    property string serverUrl: "http://127.0.0.1:8080"
    property int production: NaN
	property double productionToday: NaN
    
    // user settings from config file
	property variant gh14solarSettingsJson : {
		'serverUrl': ""
	}

	FileIO {
		id: gh14solarSettingsFile
		source: "file:///mnt/data/tsc/gh14solar.userSettings.json"
 	}
   
    function init() {
        console.log("Gh14solarApp: init")
        registry.registerWidget( "tile", tileUrl, this, null, 
         { thumbLabel: qsTr("GH14Energy"), 
           thumbIcon: thumbnailIcon, 
           thumbCategory: "general", 
           thumbWeight: 30, 
           baseTileWeight: 10, 
           thumbIconVAlignment: "center" } );
        registry.registerWidget("screen", detailScreenUrl, this, "gh14solarDetailsScreen");
        registry.registerWidget("screen", settingScreenUrl, this, "gh14solarSettingsScreen");
    }
    
    Component.onCompleted: {
		//read user settings
        try {
			gh14solarSettingsJson = JSON.parse(gh14solarSettingsFile.read());
            serverUrl = gh14solarSettingsJson.serverUrl;
            console.log("Gh14solarApp: read serverUrl from cfg " + serverUrl);
        } catch(e) {
            console.log("Gh14solarApp: failed to read user settings");
        }
    }
    
    function saveSettings() {
		
		// save user settings
 		var tmpUserSettingsJson = {
			"serverUrl": serverUrl
		}

  		var doc3 = new XMLHttpRequest();
   		doc3.open("PUT", "file:///mnt/data/tsc/gh14solar.userSettings.json");
   		doc3.send(JSON.stringify(tmpUserSettingsJson ));
	}

	function update() {
        console.log("Gh14solarApp: start update")
        var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					var json = JSON.parse( xmlhttp.responseText );
                    production = json.currentProduction;
					productionToday = json.production.today;
                    
				} else {
                    // Something went wrong, set counters to NaN
                    production = NaN;
					productionToday = NaN;
                    console.log("Gh14solarApp: failed to collect readings, server error code " + xmlhttp.status + ", message: " + xmlhttp.statusText)
                }
            }
        }
		xmlhttp.open("GET", serverUrl + serverProductionPath, true);
		xmlhttp.send();
	}

    Timer {
        id: datetimeTimer
        interval: 10000  // update every 10 seconds
        triggeredOnStart: true
        running: true
        repeat: true
        onTriggered: update()
    }
   
}
