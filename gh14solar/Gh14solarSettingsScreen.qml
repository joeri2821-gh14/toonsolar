import QtQuick 2.1
import qb.components 1.0
import qb.base 1.0
import BasicUIControls 1.0
import BxtClient 1.0

Screen {
	id: gh14solarSettingsScreen
    screenTitle: "Instellingen"
    
    function saveIP(text) {
		if (text) {
			console.log("Gh14solarApp: saving new server URL: " + text);
            app.serverUrl = text
			ipLabel.inputText = app.serverUrl;
	   		app.saveSettings();
		}
	}
    
    onShown: {
		ipLabel.inputText = app.serverUrl;
    }
    
    EditTextLabel4421 {
		id: ipLabel
		width: 512
		height: 35
		leftText: "Server URL:"
		leftTextAvailableWidth: 140

		anchors {
			left: parent.left
			leftMargin: 40
			top: parent.top
			topMargin: 30
		}
	}

	IconButton {
		id: ipButton
		width: 40

		iconSource: "qrc:/tsc/edit.png"

		anchors {
			left: ipLabel.right
			leftMargin: 6
			top: ipLabel.top
		}

		bottomClickMargin: 3
		onClicked: {
			qkeyboard.open("Server URL invoeren", app.serverUrl, saveIP);
		}
	}
    
}