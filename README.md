# Root
https://ehoco.nl/rooten-van-enecos-toon-thermostaat/
then
https://www.domoticaforum.eu/viewtopic.php?f=100&t=11235

# References
* https://www.domoticaforum.eu/viewforum.php?f=94

* https://www.domoticaforum.eu/viewtopic.php?f=103&t=11234&start=285

* https://doc.qt.io/qt-5/qtqml-javascript-hostenvironment.html

# Deploying
Copy the app and all of its contents to /HCBv2/qml/app/
on your toon, such that everything ends up in /HCBv2/qml/apps/app-name-dir/

2: On firmware from 4.16 and on:
Since firmware 4.16 there is no way to edit the globals.qml file anymore. Custom apps are now automatically started/loaded using the modded resources files if:
- there are stored in /qmf/qml/apps/app-name-dir
- the dirname start with a lowercase letter (like: /qmf/qml/apps/customApp)
- in the dir the app primary qml file is written like this 'CustomApp.qml', so it starts with a capital and it reflects the dir-name (but now with capital)

You can get the resource files from http://qutility.nl/resourcefiles or just let the update-script.sh get it for your with the -f option.

3: Restart toon's user interface by issueing:

killall qt-gui
Toon's user interface will restart, and now you can pick the tile of your application

4: All done.

# Debugging
debugging possible when running the qt-gui manually:

First edit the /usr/bin/startqt and add "exit" on the second line (after the #!/bin/sh line). This will cause the Toon not to restart qt-gui itself anymore.
Add some debugging into your app with "console.log("yourdebugtext")
Then start then qt-gui manually and only look for the debug lines you are interested in: " /HCBv2/sbin/qt-gui -platform linuxfb -plugin Tslib 2>&1 | grep -i yourdebugtext"


# Reading power production from Toon:
http://10.10.0.23/happ_pwrusage?action=GetCurrentUsage

# Reset temperature threshold behaviour
If Toon leaves a big gap between the setpoint and current temperature, reset its behaviour by deleting: `/qmf/config/config_happ_thermstat.xml`
You need to reprogramm your schedule afterwards

Deels verwijderd:
<outsideRate> - elementen
<measuredHeatingFactors>-inhoud