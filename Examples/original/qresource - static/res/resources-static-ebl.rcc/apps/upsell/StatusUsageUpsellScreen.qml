import QtQuick 2.1

UpsellAppScreen {
	screenTitle: qsTranslate("UpsellApp", "Status usage")
	screenTitleIconUrl: "../statusUsage/drawables/menuIcon.svg"

	titleText: qsTr("status-usage-upsell-title")
	bodyText: qsTr("status-usage-upsell-body")
	ctaText: qsTr("status-usage-upsell-cta")

	imageSource: "drawables/illustration-feature-status-usage.svg"
}
