import QtQuick 2.1

UpsellAppScreen {
	screenTitle: qsTranslate("UpsellApp", "Boiler Monitor")
	screenTitleIconUrl: "../boilerMonitor/drawables/app_icon.svg"

	titleText: qsTr("boiler-monitor-upsell-title")
	bodyText: qsTr("boiler-monitor-upsell-body")
	ctaText: qsTr("boiler-monitor-upsell-cta")

	imageSource: "drawables/illustration-feature-boiler-monitor.svg"
}
