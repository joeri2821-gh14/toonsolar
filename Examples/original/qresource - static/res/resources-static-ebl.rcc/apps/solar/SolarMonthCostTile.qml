import QtQuick 2.1
import qb.components 1.0

SolarMonthPerformanceTile {
	id: solarMonthCost
	production: false
	value: app.monthProducedMoney
}
