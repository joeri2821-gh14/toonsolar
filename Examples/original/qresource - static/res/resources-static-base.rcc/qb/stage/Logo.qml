import QtQuick 2.1

import qb.base 1.0
import qb.components 1.0

Widget {
	id: logo
	width: tenantlogo.width + Math.round(30 * horizontalScaling)
	height: parent.height

	Image {
		id: tenantlogo
		anchors.centerIn: parent
		source: globals.tsc["customToonLogo"] ? globals.tsc["customToonLogoURL"] : "image://scaled/images/logo" + (dimState ? "_dim" : "") + ".svg"
	}
}
