import QtQuick 2.1

import BasicUIControls 1.0;

StyledRectangle {
	id: root
	radius: designElements.radius
	mouseEnabled: false

	leftClickMargin: 10
	rightClickMargin: 10
	topClickMargin: 10
	bottomClickMargin: 10
}
