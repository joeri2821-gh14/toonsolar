import QtQuick 2.1

UpsellAppScreen {
	screenTitle: qsTranslate("UpsellApp", "Graphs")
	screenTitleIconUrl: "../graph/drawables/graphs.svg"

	titleText: qsTr("graphs-upsell-title")
	bodyText: qsTr("graphs-upsell-body")
	ctaText: qsTr("graphs-upsell-cta")

	imageSource: "drawables/illustration-feature-graphs.svg"
}
