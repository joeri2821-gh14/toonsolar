import QtQuick 2.1

BenchmarkTile {
	headTextContent: qsTr("Benchmark gas yesterday")
	type: "gas"
	unit: "m³"
}
