import QtQuick 2.1

BenchmarkTile {
	headTextContent: qsTr("Friends gas yesterday")
	type: "gas"
	unit: "m³"
	compareToFriends: true
}
