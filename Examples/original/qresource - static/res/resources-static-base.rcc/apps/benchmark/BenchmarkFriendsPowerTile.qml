import QtQuick 2.1

BenchmarkTile {
	headTextContent: qsTr("Friends power yesterday")
	type: "elec"
	unit: "kWh"
	compareToFriends: true
}
