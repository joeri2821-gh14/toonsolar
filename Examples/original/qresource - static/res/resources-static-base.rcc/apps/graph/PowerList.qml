import QtQuick 2.1

Item {
	id: powerBar
	width: childrenRect.width
	height: childrenRect.height

	Rectangle {
		id: powerBlock9
		x: 0
		y: Math.round(72 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: powerBlock8
		x: 0
		y: Math.round(64 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: powerBlock7
		x: 0
		y: Math.round(56 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: powerBlock6
		x: 0
		y: Math.round(48 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: powerBlock5
		x: 0
		y: Math.round(40 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: powerBlock4
		x: 0
		y: Math.round(32 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: powerBlock3
		x: 0
		y: Math.round(24 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: powerBlock2
		x: 0
		y: Math.round(16 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: powerBlock1
		x: 0
		y: Math.round(8 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: powerBlock0
		x: 0
		y: 0
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}
}
