import QtQuick 2.1
import qb.components 1.0

Screen {
	id: weatherSelectLocationScreen
	hasCancelButton: true
	screenTitle: qsTr("Edit Location")

	Rectangle {
		id: container
		height: Math.round(151 * verticalScaling)
		width: Math.round(400 * horizontalScaling)
		color: colors.canvas
		anchors {
			centerIn: weatherSelectLocationScreen
		}

		Text {
			id: textCountry
			text: qsTr("Edit country")
			width: Math.round(169 * horizontalScaling)
			font {
				family: qfont.semiBold.name
				pixelSize: qfont.navigationTitle
			}
			color: colors.weatherLocationHeader
		}

		Rectangle {
			id: recCountry
			width: Math.round(358 * horizontalScaling)
			height: Math.round(36 * verticalScaling)

			color: colors.weatherLocationBoxBackground
			radius: designElements.radius
			anchors {
				left: textCountry.left
				top: textCountry.bottom
			}
			Text {
				id: txtCountry
				text: app.countryName
				height: recCountry.height; width: recCountry.width
				anchors {
					left: recCountry.left
					right: recCountry.right
					leftMargin: designElements.hMargin10
					rightMargin: Math.round(2 * horizontalScaling)
				}
				font {
					family: qfont.regular.name
					pixelSize: qfont.bodyText
				}
				color: colors.weatherLocationSelected
				horizontalAlignment: Text.AlignLeft
				verticalAlignment: Text.AlignVCenter
			}
		}
		IconButton {
			id: butEditCountry
			iconSource: "qrc:/images/edit.svg"
			anchors {
				left: recCountry.right
				leftMargin: designElements.hMargin6
				top: recCountry.top
			}
			onClicked: {
				stage.openFullscreen(app.weatherCountrySelectionScreenUrl);
			}
		}

		Text {
			id: textCity
			text: qsTr("Edit location")
			width: Math.round(169 * horizontalScaling)
			font {
				family: qfont.semiBold.name
				pixelSize: qfont.navigationTitle
			}
			anchors {
				top: recCountry.bottom
				topMargin: designElements.vMargin15
			}
			color: colors.weatherLocationHeader
		}

		Rectangle {
			id: recCity
			width: Math.round(358 * horizontalScaling)
			height: Math.round(36 * verticalScaling)

			color: colors.weatherLocationBoxBackground
			radius: designElements.radius
			anchors {
				left: textCity.left
				top: textCity.bottom
			}
			Text {
				id: txtCity
				text: app.cityName
				height: recCity.height; width: recCity.width
				anchors {
					left: recCity.left
					right: recCity.right
					leftMargin: designElements.hMargin10
					rightMargin: Math.round(2 * horizontalScaling)
				}
				font {
					family: qfont.regular.name
					pixelSize: qfont.bodyText
				}
				color: colors.weatherLocationSelected
				horizontalAlignment: Text.AlignLeft
				verticalAlignment: Text.AlignVCenter
			}
			IconButton {
				id: butEditCity
				iconSource: "qrc:/images/edit.svg"
				anchors {
					left: recCity.right
					leftMargin: designElements.hMargin6
					top: recCity.top
				}
				onClicked: {
					stage.openFullscreen(app.weatherCitySelectionScreenUrl, {countryName: app.countryName, countryCode: app.countryCode});
				}
			}
		}
	}
}
