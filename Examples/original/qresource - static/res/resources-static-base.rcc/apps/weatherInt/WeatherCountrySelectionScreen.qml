import QtQuick 2.11

import qb.components 1.0
import BxtClient 1.0
import BasicUIControls 1.0

import WeatherDataModels 1.0
import SortFilterProxyModel 0.1

Screen {
	id: weatherCountryScreen
	hasCancelButton: true
	inNavigationStack: false
	screenTitle: qsTr("Choose your country")

	property int itemsPerPage: 4
	property int itemHeight: Math.round(36 * verticalScaling)

	QtObject {
		id: p
		property variant selectedCode
		property variant selectedName
		property bool fetching: false
		onSelectedCodeChanged: p.selectedCode ? enableCustomTopRightButton() : disableCustomTopRightButton()

		function getCountriesCallback(message) {
			if (!weatherCountryScreen)
				return;

			if (message) {
				var success = message.getArgument("success");
				if (success) {
					var filePath = message.getArgument("filePath");
					countryModel.parse(filePath);
				} else {
					p.fetching = false;
				}
			} else {
				p.fetching = false;
			}
		}
	}

	onShown: {
		addCustomTopRightButton(qsTr("Next"));
		disableCustomTopRightButton();
		p.selectedCode = app.countryCode;
		p.selectedName = app.countryName;
		app.getCountryList(p.getCountriesCallback);
		p.fetching = true;
		editText.text = p.selectedName;
		editText.forceActiveFocus();
		editText.selectAll();
		screenStateController.screenColorDimmedIsReachable = false;
	}

	onHidden: {
		screenStateController.screenColorDimmedIsReachable = true;
	}

	onCustomButtonClicked: {
		stage.openFullscreen(app.weatherCitySelectionScreenUrl, {countryCode: p.selectedCode, countryName: p.selectedName});
	}

	WeatherCountryModel {
		id: countryModel
		onParseComplete: {
			p.fetching = false;
			if (p.selectedCode) {
				var countryIdx = countryModel.indexByCode(p.selectedCode);
				var listIdx = countryProxyModel.mapFromSource(countryIdx);
				scrollBar.currentIndex = listIdx;
				countryList.positionViewAtIndex(scrollBar.currentIndex, ListView.Beginning);
				if (countryList.atYEnd)
					scrollBar.currentIndex = countryList.count - itemsPerPage;
				editText.text = p.selectedName;
				if (editText.activeFocus)
					editText.selectAll();
			}
		}
	}

	SortFilterProxyModel {
		id: countryProxyModel
		sourceModel: countryModel
		sortRoleName: "name"
		sortCaseSensitivity: Qt.CaseInsensitive
		sortOrder: Qt.AscendingOrder
		filterRoleName: "name"
		filterPatternSyntax: SortFilterProxyModel.RegExp
		filterCaseSensitivity: Qt.CaseInsensitive
	}

	StyledRectangle {
		id: editTextBg
		width: Math.round(400 * horizontalScaling)
		height: itemHeight
		anchors {
			horizontalCenter: parent.horizontalCenter
			top: parent.top
			topMargin: Qt.inputMethod.visible ? 0 : Math.round(50 * verticalScaling)
		}
		radius: designElements.radius
		color: colors._middlegrey

		TextInput {
			id: editText
			anchors {
				fill: parent
				margins: Math.round(8 * verticalScaling)
			}
			font {
				family: qfont.regular.name
				pixelSize: qfont.bodyText
			}
			color: colors._harry
			selectionColor: colors._branding
			selectedTextColor: colors.white

			onTextEdited: {
				countryProxyModel.filterPattern = "^" + editText.text;
				p.selectedCode = undefined;
				p.selectedName = undefined;
			}
		}

		Throbber {
			width: height
			height: parent.height
			anchors {
				right: parent.right
				verticalCenter: parent.verticalCenter
			}
			animate: visible
			visible: p.fetching

			smallRadius: 1.5
			mediumRadius: 2
			largeRadius: 2.5
			bigRadius: 3
		}
	}

	ListView {
		id: countryList
		height: (itemHeight * itemsPerPage) + (spacing * (itemsPerPage - 1))
		anchors {
			top: editTextBg.bottom
			topMargin: Math.round(4 * verticalScaling)
			left: editTextBg.left
			right: editTextBg.right
		}
		spacing: Math.round(4 * verticalScaling)
		clip: true
		interactive: false
		model: countryProxyModel
		delegate: StyledRectangle {
			id: listDelegate
			anchors.left: parent.left
			anchors.right: parent.right
			height: itemHeight
			radius: designElements.radius
			color: colors.white
			property bool isCurrentItem: p.selectedCode === model.code
			onClicked: {
				p.selectedCode = model.code;
				p.selectedName = model.name;
				editText.text = p.selectedName;
			}

			Text {
				anchors {
					left: parent.left
					right: parent.right
					leftMargin: Math.round(10 * horizontalScaling)
					rightMargin: anchors.leftMargin
					verticalCenter: parent.verticalCenter
				}
				font {
					pixelSize: qfont.bodyText
					family: isCurrentItem ? qfont.semiBold.name : qfont.regular.name
				}
				text: model.name
				color: isCurrentItem ? colors._branding : colors._gandalf
				elide: Text.ElideRight
			}
		}
		onCountChanged: {
			scrollBar.currentIndex = 0;
			positionViewAtBeginning();
			if (count === 0)
				contentHeight = 0;
		}
	}

	ScrollBar {
		id: scrollBar
		anchors {
			top: editTextBg.top
			bottom: countryList.bottom
			left: countryList.right
			leftMargin: designElements.hMargin15
		}
		container: countryList
		laneColor: colors.white
		buttonSize: itemHeight
		property int currentIndex: 0
		onNext: {
			currentIndex = Math.min(currentIndex + itemsPerPage, countryList.count - 1);
			countryList.positionViewAtIndex(currentIndex, ListView.Beginning);
			if (countryList.atYEnd)
				currentIndex = countryList.count - itemsPerPage;
		}
		onPrevious: {
			currentIndex = Math.max(currentIndex - itemsPerPage, 0);
			countryList.positionViewAtIndex(currentIndex, ListView.Beginning);
			if (countryList.atYBeginning)
				currentIndex = 0;
		}
	}
}
