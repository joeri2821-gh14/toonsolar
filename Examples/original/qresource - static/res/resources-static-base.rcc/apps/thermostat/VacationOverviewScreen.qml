
import QtQuick 2.1
import qb.components 1.0

/// Overview screen for vacation.

Screen {
	id: vacationOverviewScreen
	screenTitleIconUrl: "drawables/vacation.svg"
	screenTitle: qsTr("Vacation")

	anchors.verticalCenter: parent.verticalCenter
	anchors.horizontalCenter: parent.horizontalCenter

	QtObject {
		id: p

		function onVacationDataChanged() {
			txtTempValue.text = i18n.number(app.vacationData['temperature'], 1) + "°";
			txtFromValue.text = app.formatDateTime(app.vacationData['startTime'], true);
			txtToValue.text = app.formatDateTime(app.vacationData['endTime'], false);
			if (!app.hasVacation)
				vacationDataSavedText.visible = false;
		}

		function vacationDataSet() {
			vacationDataSavedText.visible = true;
		}
	}

	onShown: {
		app.vacationDataChanged.connect(p.onVacationDataChanged);
		app.vacationSet.connect(p.vacationDataSet);
		p.onVacationDataChanged();
	}

	onHidden: {
		vacationDataSavedText.visible = false;
		app.vacationDataChanged.disconnect(p.onVacationDataChanged);
	}

	Component.onDestruction: {
		app.vacationSet.disconnect(p.vacationDataSet);
	}

	Image {
		id: vacationImg
		anchors {
			horizontalCenter: parent.horizontalCenter
			top: parent.top
			topMargin: Math.round((app.hasVacation ? 70 : 99) * verticalScaling)
		}
		source: "image://scaled/apps/thermostat/drawables/vacation.svg"
		sourceSize.height: Math.round(86 * verticalScaling)
	}

	Item {
		id: itemNoVacation
		visible: !app.hasVacation
		anchors {
			top: vacationImg.bottom
			horizontalCenter: parent.horizontalCenter
		}

		Text {
			id: txtNoVacation
			text: qsTr("No vacation scheduled yet")
			color: colors.tpInfoLabel
			anchors {
				horizontalCenter: parent.horizontalCenter
				baseline: parent.top
				baselineOffset: Math.round(36 * verticalScaling)
			}
			font {
				family: qfont.semiBold.name
				pixelSize: qfont.navigationTitle
			}
		}

		StandardButton {
			id: butSetVacation
			text: qsTr("Set")
			anchors {
				top: txtNoVacation.baseline
				topMargin: Math.round(22 * verticalScaling)
				horizontalCenter: parent.horizontalCenter
			}
			onClicked: {
				stage.openFullscreen(app.vacationSetScreenUrl, {newVacation: true});
			}
		}
	}

	Item {
		id: vacationSet
		visible: app.hasVacation
		width: childrenRect.width
		height: childrenRect.height
		anchors {
			top: vacationImg.bottom
			horizontalCenter: parent.horizontalCenter
		}

		Text {
			id: txtTemperature
			text: qsTr("Set to")
			width: Math.round(169 * horizontalScaling)
			font {
				family: qfont.semiBold.name
				pixelSize: qfont.navigationTitle
			}
			anchors {
				left: parent.left
				baseline: parent.top
				baselineOffset: Math.round(43 * verticalScaling)
			}
			color: colors.tpInfoLabel
		}
		Text {
			id: txtFrom
			text: qsTr("From")
			width: Math.round(169 * horizontalScaling)
			font {
				family: qfont.semiBold.name
				pixelSize: qfont.navigationTitle
			}
			anchors {
				left: txtTemperature.right
				leftMargin: Math.round(26 * horizontalScaling)
				top: txtTemperature.top
			}
			color: colors.tpInfoLabel
		}
		Text {
			id: txtTo
			text: qsTr("To")
			width: Math.round(169 * horizontalScaling)
			font {
				family: qfont.semiBold.name
				pixelSize: qfont.navigationTitle
			}
			anchors {
				left: txtFrom.right
				leftMargin: Math.round(26 * horizontalScaling)
				top: txtTemperature.top
			}
			color: colors.tpInfoLabel
		}

		Rectangle {
			id: recTemperature
			width: Math.round(169 * horizontalScaling)
			height: Math.round(36 * verticalScaling)

			color: colors.vacationValueBckg
			radius: designElements.radius
			anchors {
				left: txtTemperature.left
				top: txtTemperature.baseline
				topMargin: Math.round(10 * verticalScaling)
			}
			Text {
				id: txtTempValue
				objectName: "vacationOverviewScreen_temperatureValue"
				text: i18n.number(app.vacationData.temperature, 1) + "°"
				anchors {
					fill: parent
					leftMargin: Math.round(8 * horizontalScaling)
				}
				font {
					family: qfont.regular.name
					pixelSize: qfont.titleText
				}
				color: colors.tpInfoLabel
				horizontalAlignment: Text.AlignLeft
				verticalAlignment: Text.AlignVCenter
			}
		}

		Rectangle {
			id: recFrom
			width: Math.round(169 * horizontalScaling)
			height: Math.round(36 * verticalScaling)
			color: colors.vacationValueBckg
			radius: designElements.radius
			anchors {
				left: txtFrom.left
				top: recTemperature.top
			}
			Text {
				id: txtFromValue
				objectName: "vacationOverviewScreen_fromValue"
				text: app.formatDateTime(app.vacationData.startTime, true)
				height: parent.height; width: parent.width
				anchors {
					left: parent.left
					right: parent.right
					leftMargin: Math.round(8 * horizontalScaling)
					rightMargin: Math.round(2 * verticalScaling)
				}
				font {
					family: qfont.regular.name
					pixelSize: qfont.titleText
				}
				color: colors.tpInfoLabel
				horizontalAlignment: Text.AlignLeft
				verticalAlignment: Text.AlignVCenter
			}
		}

		Rectangle {
			id: recTo
			width: Math.round(169 * horizontalScaling)
			height: Math.round(36 * verticalScaling)
			color: colors.vacationValueBckg
			radius: designElements.radius
			anchors {
				left: txtTo.left
				top: recFrom.top
			}
			Text {
				id: txtToValue
				objectName: "vacationOverviewScreen_untilValue"
				text: app.formatDateTime(app.vacationData.endTime, false)
				height: parent.height
				anchors {
					left: parent.left
					right: parent.right
					leftMargin: Math.round(8 * horizontalScaling)
					rightMargin: Math.round(2 * verticalScaling)
				}
				font {
					family: qfont.regular.name
					pixelSize: qfont.titleText
				}
				color: colors.tpInfoLabel
				horizontalAlignment: Text.AlignLeft
				verticalAlignment: Text.AlignVCenter
			}
		}

		IconButton {
			id: butEdit
			objectName: "vacationOverviewScreen_butEdit"
			iconSource: "qrc:/images/edit.svg"
			anchors {
				left: recTo.right
				leftMargin: designElements.hMargin6
				top: recTo.top
			}
			onClicked: {
				stage.openFullscreen(app.vacationSetScreenUrl, {editVacation: true});
			}
		}

		IconButton {
			id: butDelete
			iconSource: "qrc:/images/delete.svg"
			anchors {
				left: butEdit.right
				leftMargin: designElements.hMargin6
				top: butEdit.top
			}
			onClicked: app.abortVacation()
		}
	}

	WarningBox {
		id: vacationDataSavedText
		width: Math.round(455 * horizontalScaling)
		height: Math.round(45 * verticalScaling)
		warningText: qsTr("Your vacation is set and saved.")
		warningIcon: "image://scaled/apps/thermostat/drawables/vacation.svg"
		anchors {
			horizontalCenter: parent.horizontalCenter
			top: vacationSet.bottom
			topMargin: Math.round(72 * verticalScaling)
		}
		visible: false
	}
}
