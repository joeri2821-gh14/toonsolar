/**
  * @brief wrapper for parseWeatherIdAndText()
  * @param forceCross : Boolean
  *  Force display of a cross indicating that there is no data available
  * @param forceDay : Boolean
  *	 If set to true, choice of day icon is forced.
  * @param imagePrefix : String
  *	 This is the first part of the filename including the path.
  * @param idString : String
  *	 A string that holds an Id coming from buienradar.nl. This is the basis of the decision what icon to pick.
  * @param textString : String
  *	 If weatherId is "0", the icon is chosen on the basis of this string.
  * @return relative path to weather icon as string
  */
function imageOrCross(forceCross, forceDay, imagePrefix, idString, textString) {
	return (forceCross) ?
				(imagePrefix + "Cross.svg") :
				parseWeatherIdAndText (forceDay, imagePrefix, idString, textString)
}

/**
 *	@brief check for valid value. If valid, convert to fixed and replace decimal point. A "°" is added.
 *         Otherwise a dash ("-") is returned.
 *	@param forceDash : Boolean
 *		A boolean that forces to return a dash instead of
 *      temperature value computed by processTemperatureValue().
 *	@param tempValue : String
 *		Can be NaN or string value holding a temperature
 *	@param precision : Int
 *		number of decimal digits
 *  @param roundToHalves : Boolean
 *		whether or not the number should be rounded to halves
 */
function processTemperatureValue(forceDash, tempValue, precision, roundToHalves) {
	if (isNaN(tempValue) || tempValue === "" || forceDash) {
		return "-";
	}
	if (roundToHalves) {
		return i18n.number(tempValue, precision, i18n.general_rounding, 0.5) + "°";
	}
	return i18n.number(tempValue, precision) + "°";
}

/**
* @brief Calculate a filename to choose weather icons, distinquishing day and night icons. If later than 16:00 local time a night icon will be chosen.
* @param forceDay : Boolean
*	 If set to true, choice of day icon is forced.
* @param sourceFileName : String
*	 This is the first part of the filename including the path.
* @param weatherId : String
*	 A string that holds an Id coming from buienradar.nl. This is the basis of the decision what icon to pick.
* @param weatherText : String
*	 If weatherId is "0", the icon is chosen on the basis of this string.
* @return relative path to weather icon as string
*/
function parseWeatherIdAndText(forceDay, sourceFileName, weatherId, weatherText) {

	var d = new Date();
	var isTodayNight = ((d.getHours() >= 16) && (!forceDay));

	switch (weatherId) {
	case 'a': sourceFileName += isTodayNight ? "ClearNight" : "Sunny";
		break;
	case 'b':
	case 'o':
	case 'r':
	case 'j':
		sourceFileName += isTodayNight ? "CloudedNight" : "SunnyIntervals";
		break;
	case 'f':
		sourceFileName += isTodayNight ? "LightRainNight" : "LightRainDay";
		break;
	case 'k':
		sourceFileName += isTodayNight ? "RainNight" : "RainDay";
		break;
	case 'h':
	case 'i':
		sourceFileName += isTodayNight ? "RainHailNight" : "RainHailDay";
		break;
	case 'g':
		sourceFileName += isTodayNight ? "Thunder Night" : "ThunderDay";
		break;
	case 'u':
		sourceFileName += isTodayNight ? "LightSnowNight" : "LightSnowDay";
		break;
	case 'd':
		sourceFileName += isTodayNight ? "FogNight" : "FogDay";
		break;
		//symbols with moon
	case 'aa':
		sourceFileName += "ClearNight";
		break;
	case 'bb':
	case 'oo':
	case 'rr':
	case 'jj':
		sourceFileName += "CloudedNight";
		break;
	case 'ff':
		sourceFileName += "LightRainNight";
		break;
	case 'kk':
		sourceFileName += "RainNight";
		break;
	case 'hh':
	case 'ii':
		sourceFileName += "RainHailNight";
		break;
	case 'gg':
		sourceFileName += "ThunderNight";
		break;
	case 'uu':
		sourceFileName += "LightSnowNight";
		break;
	case 'dd':
		sourceFileName += "FogNight";
		break;
		//just clouds
	case 'c':
	case 'p':
	case 'cc':
	case 'pp':
		sourceFileName += "Clouded";
		break;
	case 'm':
	case 'mm':
		sourceFileName += "LightRain";
		break;
	case 'l':
	case 'll':
	case 'q':
	case 'qq':
		sourceFileName += "Rain";
		break;
	case 'w':
	case 'ww':
		sourceFileName += "Sleet";
		break;
	case 's':
	case 'ss':
		sourceFileName += "Thunder";
		break;
	case 'v':
	case 'vv':
	case 'y':
	case 'yy':
		sourceFileName += "LightSnow";
		break;
	case '1':
	case '11':
	case 't':
	case 'tt':
	case 'x':
	case 'xx':
	case 'z':
	case 'zz':
		sourceFileName += "Snow";
		break;
	case 'e':
	case 'ee':
		sourceFileName += "Fog";
		break;
	case 'n':
	case 'nn':
		sourceFileName += "SlipRisk";
		break;
	case '0':
		if (weatherText !== "")
		{
			if (weatherText === "zonnig") {
				sourceFileName += "Sunny";
			} else if (weatherText === "zonnig en bewolkt") {
				sourceFileName += "SunnyIntervals";
			} else if (weatherText === "zonnig met lichte regen" || weatherText == "zonnig, bewolkt en lichte regen") {
				sourceFileName += "LightRainDay";
			} else if (weatherText === "zonnig met lichte sneeuwval" || weatherText == "zonnig, bewolkt en lichte sneeuwval") {
				sourceFileName += "LightSnowDay";
			} else if (weatherText === "heldere nacht") {
				sourceFileName += "ClearNight";
			} else if (weatherText === "heldere lucht en hier en daar bewolkt") {
				sourceFileName += "SunnyIntervals";
			} else if (weatherText === "bewolkt") {
				sourceFileName += "Clouded";
			} else if (weatherText === "bewolkt en lichte regen" || weatherText == "bewolkt met lichte regen" || weatherText == "bewolkt, mistig en lichte regen" || weatherText == "bewolkt, mistig met lichte regen") {
				sourceFileName += "LightRain";
			} else if (weatherText === "bewolkt en regen" || weatherText == "bewolkt met regen") {
				sourceFileName += "Rain";
			} else if (weatherText === "bewolkt met sneeuwval" || weatherText == "bewolkt en sneeuwval" || weatherText == "bewolkt en lichte sneeuwval") {
				sourceFileName += "Snow";
			} else if (weatherText === "mistig") {
				sourceFileName += "Fog";
			} else {
				sourceFileName += "SunnyIntervals";
			}
		}
		break;
	default:
		sourceFileName += "Cross";
	}

	return sourceFileName += ".svg"
}

