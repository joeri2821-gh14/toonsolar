import QtQuick 2.1
import qb.components 1.0

/**
 * Component representing one station in a province - a button for selecting and a "circle" representing station position within the province.
 * Upon selecting, button is set to "selected" state (stays selected) and a station "circle" is marked as selected by making the inner red circle visible
 */

Item {
	id: stationElement

	visible: false

	property int stationId
	property string stationName

	property alias button: stationElementBtn
	property alias city: stationElementUnselected
	property bool selected: false
	property variant connectedLinks: []

	signal stationClicked;

	onSelectedChanged: {
		stationElementBtn.state = stationElement.selected ? "selected" : "up";
	}

	StandardButton {
		id: stationElementBtn

		// Explicitly overwrite the values back to the original defaults, because the province images (and offsets) are not scaled (yet?).
		defaultHeight: 36
		fontPixelSize: 15
		leftMargin: 10
		rightMargin: 10
		spacing: 10

		anchors.top: parent.top;
		anchors.left: parent.left;
		onClicked: stationClicked();
		onReleased: stationElementBtn.state = stationElement.selected ? "selected" : "up";
	}

	Rectangle {
		id: stationElementUnselected
		width: 15
		height: 15

		radius: width * 0.5
		anchors.top: parent.top;
		anchors.left: parent.left;
	}

	Rectangle {
		id: stationElementSelected
		width: 11
		height: 11

		radius: width * 0.5
		visible: selected
		anchors.top: stationElementUnselected.top
		anchors.topMargin: 2
		anchors.left: stationElementUnselected.left
		anchors.leftMargin: 2
		color: colors.waStationCircleSelected
	}
}
