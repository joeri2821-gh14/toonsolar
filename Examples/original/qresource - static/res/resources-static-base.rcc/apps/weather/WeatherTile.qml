import QtQuick 2.1
import qb.components 1.0
import "weather.js" as WeatherJS

Tile {
	id: weatherTile
	property string tempVal: WeatherJS.processTemperatureValue(!app.weatherDataRead, app.weatherPartialData['temperatuurGC'], 1, true)
	property bool dimState: screenStateController.dimmedColors

	onClicked: {
		stage.openFullscreen(app.weatherScreenUrl);
	}

	function setTileTemperature() {
		tempVal = WeatherJS.processTemperatureValue(!app.weatherDataRead, app.weatherPartialData['temperatuurGC'], 1, true)
	}

	function updateData() {
		setTileTemperature()
		makeTileIconChoice()
	}

	function init() {
		//Connect to signal when we get new data or we get a failure
		app.weatherPartialDataChanged.connect(updateData);
	}

	function makeTileIconChoice() {
		var filePrefix = dimState? "image://colorized/white/apps/weather/drawables/Home": "image://scaled/apps/weather/drawables/Home";
		weatherTileIcon.source =
				WeatherJS.imageOrCross(!app.weatherDataRead, true, filePrefix,
												app.weatherPartialData['icoonactueel'],
												app.weatherPartialData['weatherDescr']);
	}

	Component.onDestruction: app.weatherPartialDataChanged.disconnect(updateData);

	onDimStateChanged: makeTileIconChoice()

	Text {
		id: weatherTileTitleText
		text: app.location
		anchors {
			baseline: parent.top
			baselineOffset: Math.round(30 * verticalScaling)
			horizontalCenter: parent.horizontalCenter
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileTitle
		}
		color: colors.waTileTitleTextColor
	}

	Image {
		id: weatherTileIcon
		source: makeTileIconChoice()
		anchors {
			horizontalCenter: parent.horizontalCenter
			verticalCenter: parent.verticalCenter
		}
		cache: false
	}

	Text {
		id: weatherTileTemperatureText
		text: tempVal
		anchors {
			baseline: parent.bottom
			baselineOffset: designElements.vMarginNeg16
			horizontalCenter: parent.horizontalCenter
		}
		font {
			family: qfont.regular.name
			pixelSize: qfont.tileText
		}
		color: colors.waTileTextColor
	}
}
