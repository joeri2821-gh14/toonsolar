import QtQuick 2.1
import "weather.js" as WeatherJS
import qb.components 1.0

/// Weather details screen.

Screen {
	id: weatherDetailsScreen
	screenTitle: qsTr("Details over %1").arg(app.location)
	onShown: {
		if (!app.weatherDataRead) {
			for(child in columnItems.children)
				if (typeof child.value !== "undefined")
					child.value = "-";
		} else {
			p.onWeatherRadarDataChanged();
			p.onWeatherStationDataChanged();
		}
	}

	function init() {
		p.initItems();
	}

	QtObject {
		id: p

		function getTimeValue(value) {
			return value < 10 ? "0" + value : value;
		}

		function initItems() {
			columnItems.itemAt(0).label = qsTr("Temperature");
			columnItems.itemAt(1).label = qsTr("Windchill");
			columnItems.itemAt(2).label = qsTr("Humidity");
			columnItems.itemAt(3).label = qsTr("Air pressure");
			columnItems.itemAt(4).label = qsTr("Visibility");
			columnItems.itemAt(5).label = qsTr("Wind");
			columnItems.itemAt(6).label = qsTr("Gusts of wind");
			columnItems.itemAt(7).label = qsTr("Sunrise");
			columnItems.itemAt(8).label = qsTr("Sunset");
		}


		function onWeatherStationDataChanged() {
			var humidity = app.weatherStationData['luchtvochtigheid'];
			var pressure = app.weatherStationData['luchtdruk'];
			var sight = app.weatherStationData['zichtmeters'];
			var sightString = (sight > 1000) ? sight / 1000 + " km" : sight + " m";
			var windSpeed = app.weatherStationData['windsnelheidBF'];
			var windGustKMH = app.weatherStationData['windstotenMS'] * 3.6; // From "m/s" to "km/h" is a factor of 3.6
			var windDirection = getWindDirection(app.weatherStationData['windrichting'])
			columnItems.itemAt(0).value = WeatherJS.processTemperatureValue(!app.weatherDataRead, app.weatherStationData['temperatuurGC'], 1, true)
			columnItems.itemAt(1).value = WeatherJS.processTemperatureValue(!app.weatherDataRead, app.getWindChill(), 1, true);
			columnItems.itemAt(2).value = isNaN(humidity) ? "-": (i18n.number(humidity, 0) + "%");
			columnItems.itemAt(3).value = isNaN(pressure) ? "-": Math.round(pressure) + " hPa";
			columnItems.itemAt(4).value = isNaN(sight) ? "-": sightString;
			columnItems.itemAt(5).value = isNaN(windSpeed) ? "-": windDirection + " " + windSpeed + " Bft";
			columnItems.itemAt(6).value = isNaN(windGustKMH) ? "-": (i18n.number(windGustKMH, 1) + " " + qsTr("km/h"));
		}

		function onWeatherRadarDataChanged() {
			columnItems.itemAt(7).value = i18n.dateTime(app.weatherRadarData['sunrise'], i18n.time_yes | i18n.date_no | i18n.secs_no | i18n.hour_str_yes | i18n.leading_0_yes);
			columnItems.itemAt(8).value = i18n.dateTime(app.weatherRadarData['sunset'], i18n.time_yes | i18n.date_no | i18n.secs_no | i18n.hour_str_yes | i18n.leading_0_yes);
		}

		function getWindDirection(xmlWindDir) {
			switch (xmlWindDir) {
			case "N": return qsTr("North");
			case "NNO": return qsTr("North-northeast");
			case "NO": return qsTr("Northeast");
			case "ONO": return qsTr("East-northeast");
			case "O": return qsTr("East");
			case "OZO": return qsTr("East-southeast");
			case "ZO": return qsTr("Southeast");
			case "ZZO": return qsTr("South-southeast");
			case "Z": return qsTr("South");
			case "ZZW": return qsTr("South-southwest");
			case "ZW": return qsTr("Southwest");
			case "WZW": return qsTr("West-southwest");
			case "W": return qsTr("West");
			case "WNW": return qsTr("West-northwest");
			case "NW": return qsTr("Northwest");
			case "NNW": return qsTr("North-northwest");
			}
			console.log("Unsupported wind direction:", xmlWindDir);
			return "";
		}
	}

	Row {
		anchors {
			top: parent.top
			topMargin: Math.round(25 * verticalScaling)
			horizontalCenter: parent.horizontalCenter
		}
		spacing: Math.round(30 * horizontalScaling)

		RadarImage {
			size: dataColumn.height
		}

		// displaying items on the right side
		Column {
			id: dataColumn
			width: Math.round(310 * horizontalScaling)
			spacing: Math.round(6 * horizontalScaling)

			Repeater {
				id: columnItems
				model: 9

				Rectangle {
					property alias label: labelText.text
					property alias value: valueText.text

					width: parent.width
					height: Math.round(35 * verticalScaling)
					color: colors.contentBackground
					radius: designElements.radius

					Text {
						id: labelText
						anchors.left: parent.left
						anchors.leftMargin: Math.round(8 * horizontalScaling)
						anchors.verticalCenter: parent.verticalCenter

						font.pixelSize: qfont.bodyText
						font.family: qfont.regular.name
						color: colors.waBody01up
					}

					Text {
						id: valueText
						anchors.right: parent.right
						anchors.rightMargin: 16
						anchors.verticalCenter: parent.verticalCenter

						font.pixelSize: qfont.bodyText
						font.family: qfont.regular.name
						color: colors.waBody01up
					}
				}
			}
		}
	}
}
