import QtQuick 2.1
import BxtClient 1.0

import qb.components 1.0
import qb.base 1.0;

/// Weather Application.

App {
	id: weatherApp

	property url weatherScreenUrl : "WeatherScreen.qml"
	property url weatherDetailsScreenUrl : "WeatherDetailsScreen.qml"
	property url weatherCountrySelectionScreenUrl: "WeatherCountrySelectionScreen.qml"
	property url weatherCitySelectionScreenUrl: "WeatherCitySelectionScreen.qml"
	property url weatherStationScreenUrl : "WeatherStationScreen.qml"
	property url weatherSelectLocationScreenUrl : "WeatherSelectLocationScreen.qml"

	// for unit tests
	property BxtResponseHandler tst_weatherResponseHandler: weatherResponseHandler

	property int weatherUpdateNone: 0
	property int weatherUpdateFull: 1
	property int weatherUpdatePartial: 2
	property url tileUrl: "WeatherTile.qml"
	property url thumbnailIcon: "drawables/weather-thumb.svg"

	// if weather data was read correctly
	property bool weatherDataRead: false

	property bool international: false
	property string cityName
	property string countryName
	property string countryCode
	property string latitude
	property string longitude
	signal internationalSettingsChanged

	property string location
	property int locationId

	signal weatherDataReceived()

	property variant weatherStationData: {
		'id': 0,
		'luchtvochtigheid': 0,
		'temperatuurGC': 0,
		'windsnelheidMS': 0,
		'windsnelheidBF': 0,
		'windrichting': "",
		'luchtdruk': 0,
		'zichtmeters': 0,
		'windstotenMS': 0,
		'iconText': "",
		'iconID': 0,
		'iconUrl': "",
		'temperatuur10cm': 0,
		'latGraden': 0,
		'lonGraden': 0
	}

	property variant weatherRadarData: {
		'iconText': "",
		'iconID': 0,
		'iconUrl': "",
		'sunrise': "",
		'sunset': ""
	}

	property variant weatherForecastData: [{
		'dayOfWeek': "",
		'minTemperature': 0,
		'maxTemperature': 0,
		'iconID': 0,
		'iconUrl': "",
		'sneeuwcms': 0
	}]

	property string weatherSummaryData
	property string	weatherFormattedText

	property variant weatherPartialData: {
		'temperatuurGC': 0,
		'icoonactueel': "",
		'weatherDescr': ""
	}

	property variant selectedCountry
	property variant selectedCity

	property variant radarImages: []
	property int    actualImageIndex: 0
	property string actualImageSource: ""
	property string actualImageTime: ""

	QtObject {
		id: p
		property url menuImageUrl: "drawables/weather.svg"
		property string weatherUuid

		function onRadarImages(node) {
			if (node && node.getArgument("interval") !== "") {
				var now = new Date();
				var minutes = now.getMinutes();
				if (minutes < 15)      minutes = 15;
				else if (minutes < 30) minutes = 30;
				else if (minutes < 45) minutes = 45;
				else                   minutes = 60;
				now.setMinutes(minutes, 0, 0);

				var interval = node.getArgument("interval") ? node.getArgument("interval") : 10;
				now.setMinutes(now.getMinutes() + (radarImages.length * interval));

				var radarImage = {};
				radarImage['imageUrl'] = "file:///tmp/happ_weather/" + node.getArgument("image") + ".png";
				radarImage['time'] = i18n.dateTime(now, i18n.date_no | i18n.time_yes | i18n.secs_no | i18n.leading_0_yes);

				var images = radarImages;
				images.push(radarImage);
				radarImages = images;

				// start animation
				if (radarImages.length === 2) timerRadarImages.restart();
			}
		}

		function onTimerRadarImagesTriggered() {
			actualImageSource = radarImages[actualImageIndex]['imageUrl'];
			actualImageTime = radarImages[actualImageIndex]['time'];
			if (++actualImageIndex >= radarImages.length) actualImageIndex = 0;
		}

	}

	function init() {
		registry.registerWidget("screen", weatherScreenUrl, weatherApp, null, {lazyLoadScreen: true});
		registry.registerWidget("menuItem", null, weatherApp, null, {objectName: "weatherMenuItem", label: qsTr("Weather"), image: p.menuImageUrl, screenUrl: weatherScreenUrl, weight: 110});
		registry.registerWidget("screen", weatherDetailsScreenUrl, weatherApp, null, {lazyLoadScreen: true});
		registry.registerWidget("screen", weatherStationScreenUrl, weatherApp, null, {lazyLoadScreen: true});
		registry.registerWidget("screen", weatherCountrySelectionScreenUrl, weatherApp, "weatherCountrySelectionScreen", {lazyLoadScreen: true});
		registry.registerWidget("screen", weatherCitySelectionScreenUrl, weatherApp, "weatherCitySelectionScreen", {lazyLoadScreen: true});
		registry.registerWidget("screen", weatherSelectLocationScreenUrl, weatherApp, "weatherSelectLocationScreen", {lazyLoadScreen: true});
		registry.registerWidget("tile", tileUrl, weatherApp, "weatherTile", {thumbLabel: qsTr("Weather"), thumbIcon: thumbnailIcon, thumbCategory: "general", thumbWeight: 10, baseTileWeight: 20, thumbIconVAlignment: "center"});
	}

	function getLocation() {
		var msg = bxtFactory.newBxtMessage(BxtMessage.ACTION_INVOKE, p.weatherUuid, "specific1", "GetLocation");
		bxtClient.sendMsg(msg);
	}

	function getWeatherUpdate(type) {
		var msg = bxtFactory.newBxtMessage(BxtMessage.ACTION_INVOKE, p.weatherUuid, "specific1", "RegisterWeather");
		msg.addArgument("updateType", type);
		bxtClient.sendMsg(msg);
	}

	function stopRadarTimer() {
		timerRadarImages.stop();
	}

	function getWindChill() {
		var temperature = weatherStationData['temperatuurGC'];
		if (isNaN(temperature))
			return temperature;
		var windSpeed = weatherStationData['windsnelheidMS'];
		var humidity = weatherStationData['luchtvochtigheid'];
		var result;
		// FT-355 - Adding a sanity check to the windchill factor. If the windspeed is
		// larger than 20 m/s (storm strength winds), we no longer report the windchill.
		// We suspect that the weather service may report values like this by accident,
		// leading to unrealistic windchill temperatures.
		if (windSpeed > 20.0) {
			return Number.NaN
		}

		result = temperature
		if (windSpeed > 1.2 && temperature <= 10) {
			var windVar = (3.6 * windSpeed);
			result = (13.12 + (0.6215 * temperature) - (11.37 * Math.pow(windVar, 0.16)) + (0.3965 * temperature * Math.pow(windVar, 0.16)));
		}

		result = (Math.round(result * 10)) / 10;
		return Math.round(result * 2) / 2;
	}

	function getRadarImages() {
		clearRadarImages();
		var msg = bxtFactory.newBxtMessage(BxtMessage.ACTION_INVOKE, p.weatherUuid, "specific1", "GetRadarImage");
		bxtClient.sendMsg(msg);
	}

	function clearRadarImages() {
		radarImages = [];
		actualImageIndex = 0;
		actualImageSource = "";
		actualImageTime = "";
		timerRadarImages.stop();
	}

	function onLocationResponse(node) {
		if (node) {
			location = node.getArgument("location").replace(/_/g," ");
			locationId = node.getArgument("locationId");
		}
	}

	function onWeatherUpdate(node) {
		weatherDataRead = parseFloat(node.getArgument("retval")) === 0;
		if (node) {
			var childNode, i;

			// weatherStationData
			var actNode = node.getArgumentXml("weerstation");
			if (actNode) {
				var station = weatherStationData;
				station['id'] = parseInt(actNode.getAttribute("id"));

				childNode = actNode.child;
				while (childNode) {
					if (childNode.name === "icoonactueel") {
						station['iconText'] = childNode.getAttribute("zin");
						station['iconID'] = childNode.getAttribute("ID");
						station['iconUrl'] = childNode.text;
					} else if (childNode.name === "windrichting") {
						station['windrichting'] = childNode.text;
					} else {
						station[childNode.name] = parseFloat(childNode.text);
					}
					childNode = childNode.sibling;
				}
				weatherStationData = station;
			}

			// weatherRadarData
			actNode = node.getArgumentXml("buienradar");
			if (actNode) {
				var radar = weatherRadarData;
				childNode = actNode.getChild("icoonactueel");
				radar['iconText'] = childNode.getAttribute("zin");
				radar['iconID'] = childNode.getAttribute("ID");
				radar['iconUrl'] = childNode.text;
				var weatherDateFormat = "MM/dd/yyyy hh:mm:ss";
				radar['sunrise'] = qtUtils.stringToDate(actNode.getChildText("zonopkomst"), weatherDateFormat);
				radar['sunset'] = qtUtils.stringToDate(actNode.getChildText("zononder"), weatherDateFormat);
				weatherRadarData = radar;
			}

			// weatherForecastData
			actNode = node.getArgumentXml("verwachting_meerdaags");
			if (actNode) {
				var forecast = [];

				var forecastItem = {};
				childNode = actNode.getChild("dag-plus0");
				if (childNode) {
					forecastItem['minTemperature'] = parseFloat(childNode.getChildText("mintemp"));
					forecastItem['maxTemperature'] = parseFloat(childNode.getChildText("maxtemp"));
				} else {
					forecastItem['minTemperature'] = "-";
					forecastItem['maxTemperature'] = "-";
				}
				forecastItem['iconID'] = weatherRadarData['iconID'];
				forecast.push(forecastItem);

				for (i = 1; i < 6; i++) {
					childNode = actNode.getChild("dag-plus" + i);
					if (childNode) {
						forecastItem = {};
						forecastItem['minTemperature'] = parseFloat(childNode.getChildText("mintemp"));
						forecastItem['maxTemperature'] = parseFloat(childNode.getChildText("maxtemp"));
						forecastItem['iconID'] = childNode.getChild("icoon").getAttribute("ID");
						forecastItem['sneeuwcms'] = parseFloat(childNode.getChildText("sneeuwcms"));
						forecast.push(forecastItem);
					}
				}
				weatherForecastData = forecast;
			}

			// expectedToday
			actNode = node.getArgumentXml("verwachting_vandaag");
			if (actNode) {
				weatherSummaryData = actNode.getChildText("samenvatting");
				weatherFormattedText = actNode.getChildText("formattedtekst");
			}

			// partial response
			if (node.getArgumentXml("temperatuurGC")) {
				var partial = weatherPartialData;
				partial['temperatuurGC'] = parseFloat(node.getArgument("temperatuurGC"));
				partial['icoonactueel'] = node.getArgument("icoonactueel");
				partial['weatherDescr'] = node.getArgument("weatherDescr");
				weatherPartialData = partial;
			}
		}

		// clear partial data when we receive a failure
		if(!weatherDataRead)
		{
			var partial = weatherPartialData;
			partial['temperatuurGC'] = 0;
			partial['icoonactueel'] = "";
			partial['weatherDescr'] = "";
			weatherPartialData = partial;
		}

		initVarDone(0);
		// Signal there is new weather data available
		weatherDataReceived();
	}

	function storeSelectedStation(loc, locId) {
		var msg = bxtFactory.newBxtMessage(BxtMessage.ACTION_INVOKE, p.weatherUuid, null, "SetLocation");
		msg.addArgument("location", loc);
		msg.addArgument("locationId", locId);
		bxtClient.sendMsg(msg);
		locationId = locId;
		location = loc.replace(/_/g," ");
	}

	function getCountryList(callback) {
		var msg = bxtFactory.newBxtMessage(BxtMessage.ACTION_INVOKE, p.weatherUuid, "specific1", "GetCountries");
		if (callback)
			bxtClient.doAsyncBxtRequest(msg, callback, 30);
		else
			bxtClient.sendMsg(msg);
	}

	function getCityList(country, callback) {
		var msg = bxtFactory.newBxtMessage(BxtMessage.ACTION_INVOKE, p.weatherUuid, "specific1", "GetCitiesForCountry");
		msg.addArgument("countryCode", country);
		if (callback)
			bxtClient.doAsyncBxtRequest(msg, callback, 30);
		else
			bxtClient.sendMsg(msg);

	}

	function saveNewLocation(city,lat,lon, countryCode, countryName) {
		var msg = bxtFactory.newBxtMessage(BxtMessage.ACTION_INVOKE, p.weatherUuid, "specific1", "SetInternationalSettings");
		msg.addArgument("cityName", city);
		msg.addArgument("latitude", lat);
		msg.addArgument("longitude", lon);
		msg.addArgument("countryCode", countryCode);
		msg.addArgument("countryName", countryName)
		bxtClient.sendMsg(msg);
	}

	function getInternationalWeatherSettings() {
		var msg = bxtFactory.newBxtMessage(BxtMessage.ACTION_INVOKE, p.weatherUuid, "specific1", "GetInternationalSettings");
		bxtClient.sendMsg(msg);
	}

	function onInternationalSettingsResponse(message) {
		international = message.getArgument("enabled");
		cityName = message.getArgument("cityName");
		latitude = message.getArgument("latitude");
		longitude = message.getArgument("longitude");
		// For the Tile
		location = cityName;
		countryCode = message.getArgument("countryCode");
		countryName = message.getArgument("countryName");
		internationalSettingsChanged();
	}

	// 0=weatherData
	initVarCount: 1

	BxtDiscoveryHandler {
		id: weatherDiscoHandler
		deviceType: "happ_weather"
		onDiscoReceived: {
			p.weatherUuid = deviceUuid;
			getWeatherUpdate(weatherUpdatePartial);
			getLocation();
			getInternationalWeatherSettings();
		}
	}

	BxtResponseHandler {
		id: getInternationalSettingsResponseHandler
		response: "GetInternationalSettingsResponse"
		serviceId: "specific1"
		onResponseReceived: onInternationalSettingsResponse(message);
	}
	
	BxtResponseHandler {
		id: weatherResponseHandler
		response: "GetDataResponse"
		serviceId: "specific1"
		onResponseReceived: onWeatherUpdate(message)
	}

	BxtResponseHandler {
		id: weatherRadarHandler
		response: "GetRadarImageResponse"
		serviceId: "specific1"
		onResponseReceived: p.onRadarImages(message)
	}

	BxtResponseHandler {
		id: weatherLocationHandler
		response: "GetLocationResponse"
		serviceId: "specific1"
		onResponseReceived: onLocationResponse(message)
	}

	Timer {
		id: timerRadarImages
		repeat: true
		interval: 1000
		onTriggered: p.onTimerRadarImagesTriggered()
	}
}
