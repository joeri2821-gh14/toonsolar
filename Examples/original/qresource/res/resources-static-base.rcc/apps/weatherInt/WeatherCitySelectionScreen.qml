import QtQuick 2.11

import qb.components 1.0
import BxtClient 1.0
import BasicUIControls 1.0

import WeatherDataModels 1.0
import SortFilterProxyModel 0.1

Screen {
	id: weatherCityScreen
	inNavigationStack: false
	isSaveCancelDialog: true
	screenTitle: qsTr("Choose your city")
	saveEnabled: p.selectedData ? true : false

	property int itemsPerPage: 4
	property int itemHeight: Math.round(36 * verticalScaling)

	QtObject {
		id: p
		property string countryCode
		property string countryName

		property variant selectedData
		property bool fetching: false

		function getCitiesCallback(message) {
			if (!weatherCityScreen)
				return;

			if (message) {
				var success = message.getArgument("success");
				if (success) {
					var filePath = message.getArgument("filePath");
					cityModel.parse(filePath);
				} else {
					p.fetching = false;
				}
			} else {
				p.fetching = false;
			}
		}
	}

	onShown: {
		if (args) {
			p.countryName = args.countryName;
			p.countryCode = args.countryCode;
		}
		editText.text = app.cityName;
		cityProxyModel.filterPattern = "^" + editText.text;
		editText.forceActiveFocus();
		editText.selectAll();

		if (p.countryCode){
			app.getCityList(p.countryCode.toUpperCase(), p.getCitiesCallback);
			p.fetching = true;
		}
		screenStateController.screenColorDimmedIsReachable = false;
	}

	onHidden: {
		screenStateController.screenColorDimmedIsReachable = true;
	}

	onSaved: {
		app.saveNewLocation(p.selectedData.name, p.selectedData.latitude, p.selectedData.longitude, p.countryCode.toUpperCase(), p.countryName);
		stage.navigateBack();
	}

	WeatherCityModel {
		id: cityModel
		onParseComplete: {
			p.fetching = false;
			if (app.cityName) {
				var cityIdx = cityModel.indexByLatitudeLongitude(app.latitude, app.longitude);
				var listIdx = cityProxyModel.mapFromSource(cityIdx);
				if (listIdx >= 0) {
					p.selectedData = cityProxyModel.get(listIdx);
					scrollBar.currentIndex = listIdx;
					cityList.positionViewAtIndex(scrollBar.currentIndex, ListView.Beginning);
					if (cityList.atYEnd)
						scrollBar.currentIndex = cityList.count - itemsPerPage;
				}
			}
		}
	}

	SortFilterProxyModel {
		id: cityProxyModel
		sourceModel: (editText.text.trim().length >= 2) ? cityModel : null
		sortRoleName: "name"
		sortCaseSensitivity: Qt.CaseInsensitive
		sortOrder: Qt.AscendingOrder
		filterRoleName: "name"
		filterPatternSyntax: SortFilterProxyModel.RegExp
		filterCaseSensitivity: Qt.CaseInsensitive
	}

	StyledRectangle {
		id: editTextBg
		width: Math.round(400 * horizontalScaling)
		height: itemHeight
		anchors {
			horizontalCenter: parent.horizontalCenter
			top: parent.top
			topMargin: Qt.inputMethod.visible ? 0 : Math.round(50 * verticalScaling)
		}
		radius: designElements.radius
		color: colors._middlegrey

		TextInput {
			id: editText
			anchors {
				fill: parent
				margins: Math.round(8 * verticalScaling)
			}
			font {
				family: qfont.regular.name
				pixelSize: qfont.bodyText
			}
			color: colors._harry
			selectionColor: colors._branding
			selectedTextColor: colors.white

			onTextEdited: {
				p.selectedData = undefined;
				var searchString = editText.text.trim();
				if (searchString.length >= 2) {
					typeMoreText.visible = false;
					propagateEditTimer.restart();
				} else {
					typeMoreText.visible = true;
					cityList.visible = false;
					propagateEditTimer.stop();
				}
			}

			Timer {
				id: propagateEditTimer
				interval: 2000
				repeat: false

				onTriggered: {
					cityProxyModel.filterPattern = "^" + editText.text;
					cityList.visible = true;
				}
			}

			Text {
				id: typeMoreText
				visible: false

				text: qsTr("(Please type at least 2 characters)")
				color: colors._edv
				font {
					pixelSize: qfont.bodyText
					family: qfont.regular.name
				}
				anchors {
					verticalCenter: parent.verticalCenter
					right: parent.right
					rightMargin: designElements.hMargin6 + (fetchingThrobber.visible ? fetchingThrobber.width : 0)
				}
			}
		}

		Throbber {
			id: fetchingThrobber
			width: height
			height: parent.height
			anchors {
				right: parent.right
				verticalCenter: parent.verticalCenter
			}
			animate: visible
			visible: p.fetching

			smallRadius: 1.5
			mediumRadius: 2
			largeRadius: 2.5
			bigRadius: 3
		}
	}

	ListView {
		id: cityList
		height: (itemHeight * itemsPerPage) + (spacing * (itemsPerPage - 1))
		anchors {
			top: editTextBg.bottom
			topMargin: Math.round(4 * verticalScaling)
			left: editTextBg.left
			right: editTextBg.right
		}
		spacing: Math.round(4 * verticalScaling)
		clip: true
		interactive: false
		model: cityProxyModel
		delegate: StyledRectangle {
			id: listDelegate
			anchors.left: parent.left
			anchors.right: parent.right
			height: itemHeight
			radius: designElements.radius
			color: colors.white
			property bool isCurrentItem: p.selectedData ? p.selectedData.latitude === model.latitude
														  && p.selectedData.longitude === model.longitude: false
			onClicked: {
				p.selectedData = model;
				editText.text = model.name;
			}

			Text {
				anchors {
					left: parent.left
					right: parent.right
					leftMargin: Math.round(10 * horizontalScaling)
					rightMargin: anchors.leftMargin
					verticalCenter: parent.verticalCenter
				}
				font {
					pixelSize: qfont.bodyText
					family: isCurrentItem ? qfont.semiBold.name : qfont.regular.name
				}
				text: "%1 (%2)".arg(model.name).arg(model.state)
				color: isCurrentItem ? colors._branding : colors._gandalf
				elide: Text.ElideRight
			}
		}
		onCountChanged: {
			scrollBar.currentIndex = 0;
			positionViewAtBeginning();
			if (count === 0)
				contentHeight = 0;
		}
	}

	ScrollBar {
		id: scrollBar
		anchors {
			top: editTextBg.top
			bottom: cityList.bottom
			left: cityList.right
			leftMargin: designElements.hMargin15
		}
		container: cityList
		laneColor: colors.white
		buttonSize: itemHeight
		property int currentIndex: 0
		onNext: {
			currentIndex = Math.min(currentIndex + itemsPerPage, cityList.count - 1);
			cityList.positionViewAtIndex(currentIndex, ListView.Beginning);
			if (cityList.atYEnd)
				currentIndex = cityList.count - itemsPerPage;
		}
		onPrevious: {
			currentIndex = Math.max(currentIndex - itemsPerPage, 0);
			cityList.positionViewAtIndex(currentIndex, ListView.Beginning);
			if (cityList.atYBeginning)
				currentIndex = 0;
		}
	}
}
