import QtQuick 2.1

Rectangle {
	id: root

	transformOrigin: Item.TopLeft
	color: selected ? colors.waLinkSelected : colors.waLink

	property int shadowPos: shBottom
	property int shadowWidth: 1
	property int shadowOffset: 0
	property int selectedLineBorderWidth: 2
	property bool selected: false

	property int shadowXCorrection: 0
	property int shadowYCorrection: 0
	property int shadowWidthCorrection: 0
	property int shadowHeightCorrection: 0

	property int selectedShadowXCorrection: 0
	property int selectedShadowYCorrection: 0
	property int selectedShadowWidthCorrection: 0
	property int selectedShadowHeightCorrection: 0

	property int shTop: -1
	property int shBottom: 1
	property int shLeft: -2
	property int shRight: 2
	property int shInvisible: 0

	QtObject {
		id: p
		property int borderWidth: root.selected ? selectedLineBorderWidth : shadowWidth
	}

	function setCorrection(shX, shY, shW, shH, selShX, selShY, selShW, selShH) {
		shadowXCorrection = shX;
		shadowYCorrection = shY;
		shadowWidthCorrection = shW;
		shadowHeightCorrection = shH;
		selectedShadowXCorrection = selShX;
		selectedShadowYCorrection = selShY;
		selectedShadowWidthCorrection = selShW;
		selectedShadowHeightCorrection = selShH;
	}

	function setDefaultCorrection() {
		setCorrection(-1, 0, 1, 0, -1, 0, 1, 0);
	}

	function setVerticalCorrection() {
		setCorrection(0, -1, 0, 1, 0, -1, 0, 1);
	}

	function clearCorrection() {
		setCorrection(0, 0, 0, 0, 0, 0, 0, 0);
	}

	Rectangle {
		id: shadow
		visible: true
		transformOrigin: root.transformOrigin
		x: shadowXCorrection + (Math.abs(shadowPos) == 1 ? shadowOffset : (shadowPos == shLeft ? -p.borderWidth : root.width))
		y: shadowYCorrection + (shadowPos == shTop ? -p.borderWidth : (shadowPos == shBottom ? root.height : shadowOffset))
		width: shadowWidthCorrection + (Math.abs(shadowPos) == 1 ? root.width : p.borderWidth)
		height: shadowHeightCorrection + (Math.abs(shadowPos) == 1 ? p.borderWidth : root.height)
		color: root.selected ? colors.waLink : colors.waLinkShadow
	}

	Rectangle {
		id: secondShadow
		transformOrigin: root.transformOrigin
		visible: root.selected
		x: selectedShadowXCorrection + (Math.abs(shadowPos) == 1 ? shadowOffset : (shadowPos == shLeft ? root.width : -p.borderWidth))
		y: selectedShadowYCorrection + (shadowPos == shTop ? root.height : (shadowPos == shBottom ? -p.borderWidth : shadowOffset))
		width: selectedShadowWidthCorrection + shadow.width
		height: selectedShadowHeightCorrection + shadow.height
		color: colors.waLink
	}
}

