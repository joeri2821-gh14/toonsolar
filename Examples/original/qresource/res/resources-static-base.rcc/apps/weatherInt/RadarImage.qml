import QtQuick 2.1
import qb.components 1.0

Rectangle {
	id: radarBackground
	property int size: Math.round(240 * verticalScaling)
	property alias imageUrl: radarImage.source

	signal clicked()

	width: size
	height: size
	color: colors.waNoDataAvailable
	radius: designElements.radius

	Image {
		id: radarImage
		width: size
		height: size

		MouseArea {
			id: radarImageMouseArea
			anchors.fill: parent
			onClicked: radarBackground.clicked()
		}
	}

	Throbber {
		anchors.centerIn: parent
		visible: radarImage.status === Image.Loading
	}

	Text {
		id: noInfoAvailableText
		anchors.centerIn: parent
		visible: radarImage.status === Image.Error || radarImage.status === Image.Null
		font {
			pixelSize: qfont.titleText
			family: qfont.regular.name
		}
		horizontalAlignment: Text.AlignHCenter
		color: colors.menuBarLabelDown
		text: qsTr("no_weather_data_available_txt")
	}
}
