import QtQuick 2.1

UpsellAppScreen {
	screenTitle: qsTranslate("UpsellApp", "Control Panel")
	screenTitleIconUrl: "../controlPanel/drawables/controlPanelClosed.svg"

	titleText: qsTr("control-panel-upsell-title")
	bodyText: qsTr("control-panel-upsell-body")
	ctaText: qsTr("control-panel-upsell-cta")

	imageSource: "drawables/illustration-feature-control-panel.svg"
}
