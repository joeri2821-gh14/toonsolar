import QtQuick 2.1

UpsellAppScreen {
	screenTitle: qsTranslate("UpsellApp", "Weather")
	screenTitleIconUrl: "../weather/drawables/weather.svg"

	titleText: qsTr("weather-upsell-title")
	bodyText: qsTr("weather-upsell-body")
	ctaText: qsTr("weather-upsell-cta")

	imageSource: "drawables/illustration-feature-weather.svg"
}
