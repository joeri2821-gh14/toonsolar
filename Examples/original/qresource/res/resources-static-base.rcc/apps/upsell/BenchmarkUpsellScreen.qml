import QtQuick 2.1

UpsellAppScreen {
	screenTitle: qsTranslate("UpsellApp", "Benchmark")
	screenTitleIconUrl: "../benchmark/drawables/vergelijk_menu.svg"

	titleText: qsTr("benchmark-upsell-title")
	bodyText: qsTr("benchmark-upsell-body")
	ctaText: qsTr("benchmark-upsell-cta")

	imageSource: "drawables/illustration-feature-benchmark.svg"
}
