import QtQuick 2.1

UpsellAppScreen {
	screenTitle: qsTranslate("UpsellApp", "Smoke detector")
	screenTitleIconUrl: "../smokeDetector/drawables/smokedetector.svg"

	titleText: qsTr("smoke-detector-upsell-title")
	bodyText: qsTr("smoke-detector-upsell-body")
	ctaText: qsTr("smoke-detector-upsell-cta")

	imageSource: "drawables/illustration-feature-sd.svg"
}
