import QtQuick 2.1
import qb.components 1.0
import ImagePixelReader 1.0

Screen {
	id: weatherStationScreen
	screenTitle: qsTr("Weatherstation")
	isSaveCancelDialog: true
	property int offsetX: 68
	property int offsetY: 76
	property string activeProvince
	property alias zeelandImageVisible: zeelandImage.visible
	property alias utrechtImageVisible: utrechtImage.visible
	property alias gelderlandImageVisible: gelderlandImage.visible

	property variant location2province: {
		"Amsterdam":  "#f0c4f",
		"Arnhem":  "#c599ff",
		"Berkhout":  "#f0c4f",
		"Breda":  "#f14688",
		"Den Helder": "#f0c4f",
		"Eindhoven":  "#f14688",
		"Friesland West": "#d21242",
		"Goes":  "#23922d",
		"Gorinchem":  "#b9cc33",
		"Groningen":  "#85d93e",
		"Groningen Noord": "#85d93e",
		"Groningen Oost": "#85d93e",
		"Hoek van Holland": "#b9cc33",
		"Hoogeveen":  "#f39c12",
		"Leeuwarden":  "#d21242",
		"Lelystad":  "#238195",
		"Maastricht":  "#6868aa",
		"Noordoostpolder":   "#238195",
		"Gelderland Oost": "#c599ff",
		"Rotterdam":  "#b9cc33",
		"Terneuzen":  "#23922d",
		"Twente":  "#e74b3b",
		"Uden":  "#f14688",
		"Utrecht":  "#5da9d4",
		"Utrecht West": "#5da9d4",
		"Venlo":  "#6868aa",
		"Vlieland":  "#d21242",
		"Vlissingen":  "#23922d",
		"Voorschoten":  "#b9cc33",
		"Terschelling":  "#d21242",
		"Weert":  "#6868aa",
		"Woensdrecht":  "#f14688",
		"Zwolle": "#e74b3b"
	}

	property variant provincies: {
		"#23922d": {'img': zeelandImage, 'name': "zeeland"},
		"#b9cc33": {'img': zuidhollandImage, 'name': "zuidholland"},
		"#f0c4f" : {'img': noordhollandImage, 'name': "noordholland"},
		"#d21242": {'img': frieslandImage, 'name': "friesland"},
		"#85d93e": {'img': groningenImage, 'name': "groningen"},
		"#f39c12": {'img': drentheImage, 'name': "drenthe"},
		"#e74b3b": {'img': overijsselImage, 'name': "overijssel"},
		"#c599ff": {'img': gelderlandImage, 'name': "gelderland"},
		"#5da9d4": {'img': utrechtImage, 'name': "utrecht"},
		"#f14688": {'img': noordbrabantImage, 'name': "noordbrabant"},
		"#6868aa": {'img': limburgImage, 'name': "limburg"},
		"#238195": {'img': flevolandImage, 'name': "flevoland"},
		"#000": false
	}

	QtObject {
		id: p

		property string activeProvinceShortName
	}

	function setSelectedProvince() {
		if (provincies[activeProvince])
			provincies[activeProvince].img.visible = false;
		provincies[location2province[app.location]].img.visible = true;
		activeProvince = location2province[app.location];
		p.activeProvinceShortName = provincies[location2province[app.location]].name;
		provinceItem.selectDefault(p.activeProvinceShortName, app.locationId);
	}

	function init() {
		provinceItem.init();
	}

	function picProvince(x, y) {
		var activeProvinceNew = pixelReader.getPixel(x, y);
		if (provincies[activeProvinceNew]) {
			if (activeProvince)
				provincies[activeProvince].img.visible = false;
			provincies[activeProvinceNew].img.visible = true;
			activeProvince = activeProvinceNew;
			p.activeProvinceShortName = provincies[activeProvinceNew].name;
			provinceItem.showProvince(p.activeProvinceShortName);
		}
	}

	function getProvinceName(shortName) {
		switch (shortName) {
		case "zuidholland": return qsTr("Zuid-Holland");
		case "noordholland": return qsTr("Noord-Holland");
		case "zeeland": return qsTr("Zeeland");
		case "groningen": return qsTr("Groningen");
		case "drenthe": return qsTr("Drenthe");
		case "overijssel": return qsTr("Overijssel");
		case "gelderland": return qsTr("Gelderland");
		case "utrecht": return qsTr("Utrecht");
		case "noordbrabant": return qsTr("Noord-Brabant");
		case "limburg": return qsTr("Limburg");
		case "flevoland": return qsTr("Flevoland");
		case "friesland": return qsTr("Friesland");
		default: break;
		}
		return "";
	}

	onShown: {
		screenStateController.screenColorDimmedIsReachable = false;
		setSelectedProvince();
	}

	onHidden: {
		screenStateController.screenColorDimmedIsReachable = true;
	}

	onSaved: {
		app.storeSelectedStation(provinceItem.selectedStation, provinceItem.selectedStationId);
		setSelectedProvince();
	}

	onCanceled: {
		if (activeProvince)
			provincies[activeProvince].img.visible = false;
		setSelectedProvince();
	}

	Row {
		id: panelRow
		spacing: 0
		anchors.fill: parent
		Item {
			id: leftPanel
			width: weatherStationScreen.width / 2 - 10
			height: weatherStationScreen.height

			ImagePixelReader {
				id: pixelReader
				imageUrl: "drawables/provincies/nederlandcolourfullClickAreas.png"
			}

			Text {
				id: chooseProvinceText
				color: colors.waSystemState
				font {
					pixelSize: qfont.navigationTitle
					family: qfont.semiBold.name
				}
				anchors {
					baseline: parent.top
					left: parent.left
					baselineOffset: 60
					leftMargin: 68
				}
				text: qsTr("Select your province")
			}

			Image {
				id: dutchMapImage
				source: "drawables/provincies/nederland.png"
				anchors {
					top: chooseProvinceText.baseline
					topMargin: 15
					left: parent.left
					leftMargin: 68
				}
				cache: false

				MouseArea {
					id: dutchMapMouseArea
					anchors.fill: parent
					onClicked: picProvince(mouseX, mouseY)
					property string kpiPostfix: "provinceSelect"
				}
			}
			Image {
				id: drentheImage
				source: "drawables/provincies/tinydrenthe.png"
				visible: false
				x: offsetX + 184
				y: offsetY + 42
				cache: false
			}
			Image {
				id: flevolandImage
				source: "drawables/provincies/tinyflevoland.png"
				visible: false
				x: offsetX + 119
				y: offsetY + 84
				cache: false
			}
			Image {
				id: frieslandImage
				source: "drawables/provincies/tinyfriesland.png"
				visible: false
				x: offsetX + 99
				y: offsetY + 5
				cache: false
			}
			Image {
				id: gelderlandImage
				source: "drawables/provincies/tinygelderland.png"
				visible: false
				x: offsetX + 110
				y: offsetY + 122
				cache: false
			}
			Image {
				id: groningenImage
				source: "drawables/provincies/tinygroningen.png"
				visible: false
				x: offsetX + 186
				y: offsetY
			}
			Image {
				id: limburgImage
				source: "drawables/provincies/tinylimburg.png"
				visible: false
				x: offsetX + 159
				y: offsetY + 205
				cache: false
			}
			Image {
				id: noordbrabantImage
				source: "drawables/provincies/tinynoordbrabant.png"
				visible: false
				x: offsetX + 52
				y: offsetY + 201
				cache: false
			}
			Image {
				id: noordhollandImage
				source: "drawables/provincies/tinynoordholland.png"
				visible: false
				x: offsetX + 77
				y: offsetY + 45
				cache: false
			}
			Image {
				id: overijsselImage
				source: "drawables/provincies/tinyoverijssel.png"
				visible: false
				x: offsetX + 163
				y: offsetY + 82
				cache: false
			}
			Image {
				id: utrechtImage
				source: "drawables/provincies/tinyutrecht.png"
				visible: false
				x: offsetX + 99
				y: offsetY + 146
				cache: false
			}
			Image {
				id: zeelandImage
				source: "drawables/provincies/tinyzeeland.png"
				visible: false
				x: offsetX + 1
				y: offsetY + 214
				cache: false
			}
			Image {
				id: zuidhollandImage
				source: "drawables/provincies/tinyzuidholland.png"
				visible: false
				x: offsetX + 33
				y: offsetY + 145
				cache: false
			}

		}
		Item {
			id: rightPanel
			width: weatherStationScreen.width / 2 + 10
			height: weatherStationScreen.height

			Text {
				id: chooseStationText
				color: colors.waSystemState
				font {
					pixelSize: qfont.navigationTitle
					family: qfont.semiBold.name
				}
				anchors {
					left: parent.left
					right: parent.right
					baseline: parent.top
					baselineOffset: 60
				}
				text: qsTr("Choose your weather station in %1").arg(getProvinceName(p.activeProvinceShortName))
			}

			WeatherStationProvince {
				id: provinceItem
				anchors {
					top: chooseStationText.bottom
					bottom: parent.bottom
					left: parent.left
					right: parent.right

					leftMargin: 40
				}
			}
		}
	}

}
