import QtQuick 2.1
import qb.components 1.0

Rectangle {
	id: radarBackground
	property int size: Math.round(240 * verticalScaling)

	signal clicked()

	width: size
	height: size
	color: colors.waNoDataAvailable
	radius: designElements.radius

	AnimatedImage {
		id: radarImage
		width: size
		height: size
		cache: true
		Component.onCompleted: { source = "https://api.buienradar.nl/image/1.0/RadarMapNL?&hist=0&forc=60&step=2&w=" + width + "&h=" + height; }

		MouseArea {
			id: radarImageMouseArea
			anchors.fill: parent
			onClicked: radarBackground.clicked()
		}
	}

	Throbber {
		anchors.centerIn: parent
		visible: radarImage.status === Image.Loading
	}

	Text {
		id: noInfoAvailableText
		anchors.centerIn: parent
		visible: radarImage.status === Image.Error || radarImage.status === Image.Null
		font {
			pixelSize: qfont.titleText
			family: qfont.regular.name
		}
		horizontalAlignment: Text.AlignHCenter
		color: colors.menuBarLabelDown
		text: qsTr("no_weather_data_available_txt")
	}
}
