import QtQuick 2.1
import BxtClient 1.0

import qb.components 1.0
import qb.base 1.0;

/// Weather Application.

App {
	id: weatherApp

	property url weatherScreenUrl : "WeatherScreen.qml"
	property url weatherDetailsScreenUrl : "WeatherDetailsScreen.qml"
	property url weatherStationScreenUrl : "WeatherStationScreen.qml"

	// for unit tests
	property BxtResponseHandler tst_weatherResponseHandler: weatherResponseHandler

	property int weatherUpdateNone: 0
	property int weatherUpdateFull: 1
	property int weatherUpdatePartial: 2
	property url tileUrl: "WeatherTile.qml"
	property url thumbnailIcon: "drawables/weather-thumb.svg"

	// if weather data was read correctly
	property bool weatherDataRead: false

	property string location
	property int locationId

	signal weatherDataReceived()

	property variant weatherStationData: {
		'id': 0,
		'luchtvochtigheid': 0,
		'temperatuurGC': 0,
		'windsnelheidMS': 0,
		'windsnelheidBF': 0,
		'windrichting': "",
		'luchtdruk': 0,
		'zichtmeters': 0,
		'windstotenMS': 0,
		'iconText': "",
		'iconID': 0,
		'temperatuur10cm': 0,
		'latGraden': 0,
		'lonGraden': 0
	}

	property variant weatherRadarData: {
		'iconText': "",
		'iconID': 0,
		'sunrise': "",
		'sunset': ""
	}

	property variant weatherForecastData: [{
		'dayOfWeek': "",
		'minTemperature': 0,
		'maxTemperature': 0,
		'iconID': 0,
		'sneeuwcms': 0
	}]

	property string weatherSummaryData
	property string	weatherFormattedText

	property variant weatherPartialData: {
		'temperatuurGC': 0,
		'icoonactueel': "",
		'weatherDescr': ""
	}

	QtObject {
		id: p
		property url menuImageUrl: "drawables/weather.svg"
		property string weatherUuid
	}

	function init() {
		registry.registerWidget("screen", weatherScreenUrl, weatherApp, null, {lazyLoadScreen: true});
		registry.registerWidget("menuItem", null, weatherApp, null, {objectName: "weatherMenuItem", label: qsTr("Weather"), image: p.menuImageUrl, screenUrl: weatherScreenUrl, weight: 110});
		registry.registerWidget("screen", weatherDetailsScreenUrl, weatherApp, null, {lazyLoadScreen: true});
		registry.registerWidget("screen", weatherStationScreenUrl, weatherApp, null, {lazyLoadScreen: true});
		registry.registerWidget("tile", tileUrl, weatherApp, "weatherTile", {thumbLabel: qsTr("Weather"), thumbIcon: thumbnailIcon, thumbCategory: "general", thumbWeight: 10, baseTileWeight: 20, thumbIconVAlignment: "center"});
	}

	function getLocation() {
		var msg = bxtFactory.newBxtMessage(BxtMessage.ACTION_INVOKE, p.weatherUuid, "specific1", "GetLocation");
		bxtClient.sendMsg(msg);
	}

	function getWeatherUpdate(type) {
		var msg = bxtFactory.newBxtMessage(BxtMessage.ACTION_INVOKE, p.weatherUuid, "specific1", "RegisterWeather");
		msg.addArgument("updateType", type);
		bxtClient.sendMsg(msg);
	}

	function getWindChill() {
		var temperature = weatherStationData['temperatuurGC'];
		if (isNaN(temperature))
			return temperature;
		var windSpeed = weatherStationData['windsnelheidMS'];
		var humidity = weatherStationData['luchtvochtigheid'];
		var result;
		// FT-355 - Adding a sanity check to the windchill factor. If the windspeed is
		// larger than 20 m/s (storm strength winds), we no longer report the windchill.
		// We suspect that the weather service may report values like this by accident,
		// leading to unrealistic windchill temperatures.
		if (windSpeed > 20.0) {
			return Number.NaN
		}

		result = temperature
		if (windSpeed > 1.2 && temperature <= 10) {
			var windVar = (3.6 * windSpeed);
			result = (13.12 + (0.6215 * temperature) - (11.37 * Math.pow(windVar, 0.16)) + (0.3965 * temperature * Math.pow(windVar, 0.16)));
		}

		result = (Math.round(result * 10)) / 10;
		return Math.round(result * 2) / 2;
	}

	function onLocationResponse(node) {
		if (node) {
			location = node.getArgument("location").replace(/_/g," ");
			locationId = node.getArgument("locationId");

			// The Katwijk station was removed from Buienradar, but the
			// Voorschoten station is very close by.
			// If users had Katwijk configured, move/show them to the
			// Voorschoten station instead.
			if (location === "Katwijk" && locationId === 6210) {
				location = "Voorschoten"
				locationId = 6215

				storeSelectedStation(location, locationId)
			}
		}
	}

	function onWeatherUpdate(node) {
		weatherDataRead = parseFloat(node.getArgument("retval")) === 0;
		if (node) {
			var childNode, i;

			// weatherStationData
			var actNode = node.getArgumentXml("weerstation");
			if (actNode) {
				var station = weatherStationData;
				station['id'] = parseInt(actNode.getAttribute("id"));

				childNode = actNode.child;
				while (childNode) {
					if (childNode.name === "icoonactueel") {
						station['iconText'] = childNode.getAttribute("zin");
						station['iconID'] = childNode.getAttribute("ID");
					} else if (childNode.name === "windrichting") {
						station['windrichting'] = childNode.text;
					} else {
						station[childNode.name] = parseFloat(childNode.text);
					}
					childNode = childNode.sibling;
				}
				weatherStationData = station;
			}

			// weatherRadarData
			actNode = node.getArgumentXml("buienradar");
			if (actNode) {
				var radar = weatherRadarData;
				childNode = actNode.getChild("icoonactueel");
				radar['iconText'] = childNode.getAttribute("zin");
				radar['iconID'] = childNode.getAttribute("ID");
				var weatherDateFormat = "MM/dd/yyyy hh:mm:ss";
				radar['sunrise'] = qtUtils.stringToDate(actNode.getChildText("zonopkomst"), weatherDateFormat);
				radar['sunset'] = qtUtils.stringToDate(actNode.getChildText("zononder"), weatherDateFormat);
				weatherRadarData = radar;
			}

			// weatherForecastData
			actNode = node.getArgumentXml("verwachting_meerdaags");
			if (actNode) {
				var forecast = [];

				var forecastItem = {};
				childNode = actNode.getChild("dag-plus0");
				if (childNode) {
					forecastItem['minTemperature'] = parseFloat(childNode.getChildText("mintemp"));
					forecastItem['maxTemperature'] = parseFloat(childNode.getChildText("maxtemp"));
				} else {
					forecastItem['minTemperature'] = "-";
					forecastItem['maxTemperature'] = "-";
				}
				forecastItem['iconID'] = weatherRadarData['iconID'];
				forecast.push(forecastItem);

				for (i = 1; i < 6; i++) {
					childNode = actNode.getChild("dag-plus" + i);
					if (childNode) {
						forecastItem = {};
						forecastItem['minTemperature'] = parseFloat(childNode.getChildText("mintemp"));
						forecastItem['maxTemperature'] = parseFloat(childNode.getChildText("maxtemp"));
						forecastItem['iconID'] = childNode.getChild("icoon").getAttribute("ID");
						forecastItem['sneeuwcms'] = parseFloat(childNode.getChildText("sneeuwcms"));
						forecast.push(forecastItem);
					}
				}
				weatherForecastData = forecast;
			}

			// expectedToday
			actNode = node.getArgumentXml("verwachting_vandaag");
			if (actNode) {
				weatherSummaryData = actNode.getChildText("samenvatting");
				weatherFormattedText = actNode.getChildText("formattedtekst");
			}

			// partial response
			if (node.getArgumentXml("temperatuurGC")) {
				var partial = weatherPartialData;
				partial['temperatuurGC'] = parseFloat(node.getArgument("temperatuurGC"));
				partial['icoonactueel'] = node.getArgument("icoonactueel");
				partial['weatherDescr'] = node.getArgument("weatherDescr");
				weatherPartialData = partial;
			}
		}

		// clear partial data when we receive a failure
		if(!weatherDataRead)
		{
			var partial = weatherPartialData;
			partial['temperatuurGC'] = 0;
			partial['icoonactueel'] = "";
			partial['weatherDescr'] = "";
			weatherPartialData = partial;
		}

		initVarDone(0);
		// Signal there is new weather data available
		weatherDataReceived();
	}

	function storeSelectedStation(loc, locId) {
		var msg = bxtFactory.newBxtMessage(BxtMessage.ACTION_INVOKE, p.weatherUuid, null, "SetLocation");
		msg.addArgument("location", loc);
		msg.addArgument("locationId", locId);
		bxtClient.sendMsg(msg);
		locationId = locId;
		location = loc.replace(/_/g," ");
	}

	// 0=weatherData
	initVarCount: 1

	BxtDiscoveryHandler {
		id: weatherDiscoHandler
		deviceType: "happ_weather"
		onDiscoReceived: {
			p.weatherUuid = deviceUuid;
			getWeatherUpdate(weatherUpdatePartial);
			getLocation();
		}
	}

	BxtResponseHandler {
		id: weatherResponseHandler
		response: "GetDataResponse"
		serviceId: "specific1"
		onResponseReceived: onWeatherUpdate(message)
	}

	BxtResponseHandler {
		id: weatherLocationHandler
		response: "GetLocationResponse"
		serviceId: "specific1"
		onResponseReceived: onLocationResponse(message)
	}
}
