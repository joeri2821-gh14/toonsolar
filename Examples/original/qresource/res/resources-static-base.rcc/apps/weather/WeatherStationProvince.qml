import QtQuick 2.1
import qb.components 1.0

import "stations.js" as StationsJs

/**
 * Component responsible for displaying province with stations - buttons for selecting the station, circles for station position in province and a link between
 * station and its button.
 * Each province is reusing the same set of WeatherStationElement (button, circle) and Rectangles (lines between circle and button). All elements are
 * stored in arrays in stations.js to be able to iterate and modify all items. For this, init() method has to be called first.
 * The logic to have only one selected WeatherStationElement in a province is also done here.
 */

Item {
	id: root

	property int selectedStationId: -1
	property string selectedStation

	QtObject {
		id: p

		property string lastSelectedProvince
		property int lastSelected: -1
		property int selectedElementId
		property string selectedProvinceName

		property variant diagonalDirection: {
			'rightDown': 1,
			'leftDown': 2,
			'leftUp': 3,
			'rightUp': 4,
		}

		// logic for selecting only one station in a province
		function stationSelected(elementId) {
			if (p.lastSelected === elementId)
				return;

			if (p.lastSelected >= 0) {
				StationsJs.stationElements[p.lastSelected].selected = false;
				for (var i = 0; i < StationsJs.stationElements[p.lastSelected].connectedLinks.length; i++) {
					StationsJs.stationElements[p.lastSelected].connectedLinks[i].selected = false;
				}
			}

			StationsJs.stationElements[elementId].selected = true;
			for (i = 0; i < StationsJs.stationElements[elementId].connectedLinks.length; i++) {
				StationsJs.stationElements[elementId].connectedLinks[i].selected = true;
			}
			p.lastSelected = elementId;
			selectedStationId = StationsJs.stationElements[elementId].stationId;
			selectedStation = StationsJs.stationElements[elementId].stationName;
			p.selectedProvinceName = p.lastSelectedProvince;
			p.selectedElementId = elementId;
		}

		// reset all properties and hide elements
		function hideProvince() {
			provinceImg.anchors.leftMargin = 0

			for (var i = 0; i < StationsJs.stationElements.length; i++) {
				StationsJs.stationElements[i].visible = false;
				StationsJs.stationElements[i].selected = false;
				StationsJs.stationElements[i].connectedLinks = [];
			}

			for (i = 0; i < StationsJs.stationLinkElement.length; i++) {
				StationsJs.stationLinkElement[i].visible = false;
				StationsJs.stationLinkElement[i].rotation = 0;
				StationsJs.stationLinkElement[i].height = 2;
				StationsJs.stationLinkElement[i].selected = false;
				StationsJs.stationLinkElement[i].shadowPos = StationsJs.stationLinkElement[i].shBottom;
				StationsJs.stationLinkElement[i].setDefaultCorrection();
			}
			p.lastSelected = -1;
		}

		// drawing diagonal (45° angle) from absolut position x,y with length derived from x or y delta (endX - startX is equal to endY - startY)
		// and with direction either rightDown, rightUp, leftDown, leftUp
		function drawDiagonalLine(line, x, y, delta, direction) {
			var startX = 0;
			var startY = 0;
			var rotation = 0;
			if (direction === p.diagonalDirection.rightDown) {
				startX = x;
				startY = y;
				rotation = 45;
			}
			else if (direction === p.diagonalDirection.rightUp) {
				startX = x;
				startY = y;
				rotation = -45;
			}
			else if (direction === p.diagonalDirection.leftDown) {
				startX = x - delta;
				startY = y + delta;
				rotation = -45;
			}
			else if (direction === p.diagonalDirection.leftUp) {
				startX = x - delta;
				startY = y - delta;
				rotation = 45;
			}
			else {
				console.log("drawDiagonalLine: wrong direction: ", direction);
				return;
			}

			line.width = Math.sqrt(2*Math.pow((delta), 2)) + 1;
			line.rotation = rotation;
			line.x = startX;
			line.y = startY;
			line.visible = true;
		}

		// vertical space between 2 elements
		function vSpace(element1, element2) {
			var distance = 0;
			if (element1.y < element2.y)
				distance = element2.y - (element1.y + element1.height);
			else
				distance = element1.y - (element2.y + element2.height);
			return distance;
		}

		// horizontal space between 2 elements
		function hSpace(element1, element2) {
			var distance = 0;
			if (element1.x < element2.x)
				distance = element2.x - (element1.x + element1.width);
			else
				distance = element1.x - (element2.x + element2.width);
			return distance;
		}
	}

	function showProvince(name) {
		p.hideProvince();
		p.lastSelectedProvince = name;
		switch (name) {
			case "gelderland": showGelderland(); break;
			case "noordbrabant": showNoordBrabant(); break;
			case "groningen": showGroningen(); break;
			case "zeeland": showZeeland(); break;
			case "limburg": showLimburg(); break;
			case "zuidholland": showZuidHolland(); break;
			case "noordholland": showNoordHolland(); break;
			case "flevoland": showFlevoland(); break;
			case "overijssel": showOverijssel(); break;
			case "friesland": showFriesland(); break;
			case "drenthe": showDrenthe(); break;
			case "utrecht": showUtrecht(); break;
			default:
				console.log("UNKNOWN PROVINCE NAME!");
				return;
		}

		//if @name is a province where the last selected station is, make the station selected upon showing the province
		if (p.selectedProvinceName === name)
			p.stationSelected(p.selectedElementId);

		provinceImg.source = "drawables/provincies/" + name + ".png";
	}

	function selectDefault(province, stationId) {
		p.selectedProvinceName = "";
		p.selectedElementId = -1;
		selectedStationId = -1;
		showProvince(province);
		for (var i = 0; i < StationsJs.stationElements.length; i++) {
			if (StationsJs.stationElements[i].stationId === stationId) {
				p.stationSelected(i);
				break;
			}
		}
	}

	function init() {
		StationsJs.stationElements.push(stationElement0);
		StationsJs.stationElements.push(stationElement1);
		StationsJs.stationElements.push(stationElement2);
		StationsJs.stationElements.push(stationElement3);
		StationsJs.stationElements.push(stationElement4);

		StationsJs.stationLinkElement.push(linkElement0);
		StationsJs.stationLinkElement.push(linkElement1);
		StationsJs.stationLinkElement.push(linkElement2);
		StationsJs.stationLinkElement.push(linkElement3);
		StationsJs.stationLinkElement.push(linkElement4);
		StationsJs.stationLinkElement.push(linkElement5);
		StationsJs.stationLinkElement.push(linkElement6);
		StationsJs.stationLinkElement.push(linkElement7);
		StationsJs.stationLinkElement.push(linkElement8);
		StationsJs.stationLinkElement.push(linkElement9);
	}

/*
To anyone working on this: good luck!
All city/button anchor margins are specified relative to top-left of the provinceImg. This should make it easier to
find/adjust coordinates by analyzing screenshots.

Quite a few button.anchors.topMargin values use an offset of -11 compared to the city.anchors.topMargin. This value (-11) is
derived from the size of the city-indicator (15px) and the height of the button (36px) -> (15 - 36) / 2 = -10.5 -> -11

For some provinces the leftMargin of the provinceImg is adjusted to gain space on the right side of the display. This leftMargin
is reset to 0 in the p.hideProvince() function.
*/

	function showGelderland() {
		provinceImg.anchors.leftMargin = -35;

		stationElement0.stationId = 6283;
		stationElement0.stationName = "Gelderland_Oost";
		stationElement0.city.anchors.topMargin = 81
		stationElement0.city.anchors.leftMargin = 219
		stationElement0.button.anchors.topMargin = stationElement0.city.anchors.topMargin - 11
		stationElement0.button.anchors.leftMargin = 258
		stationElement0.button.bottomClickMargin = 8;
		stationElement0.button.topClickMargin = 10;
		stationElement0.button.text = qsTr("Gelderland Oost");
		stationElement0.visible = true;
		linkElement0.width = stationElement0.button.anchors.leftMargin - stationElement0.city.anchors.leftMargin - stationElement0.city.width
		linkElement0.x = stationElement0.city.x + 15;
		linkElement0.y = stationElement0.city.y + 7;
		linkElement0.visible = true;
		stationElement0.connectedLinks = [linkElement0];


		stationElement1.stationId = 6275;
		stationElement1.stationName = "Arnhem";
		stationElement1.city.anchors.topMargin = 124
		stationElement1.city.anchors.leftMargin = 118
		stationElement1.button.anchors.topMargin = stationElement1.city.anchors.topMargin
		stationElement1.button.anchors.leftMargin = stationElement0.button.anchors.leftMargin
		stationElement1.button.topClickMargin = 8;
		stationElement1.button.bottomClickMargin = 10;
		stationElement1.button.text = qsTr("Arnhem");
		stationElement1.visible = true;
		linkElement1.width = (101+28) - stationElement1.city.width;
		linkElement1.x = stationElement1.city.x + 15;
		linkElement1.y = stationElement1.city.y + 7;
		linkElement1.visible = true;
		p.drawDiagonalLine(linkElement2, linkElement1.x + linkElement1.width - 1, linkElement1.y, 11 + 1, p.diagonalDirection.rightDown);
		linkElement2.setCorrection(1, 0, 0, 0, 0, -1, 0, 1);
		stationElement1.connectedLinks = [linkElement1, linkElement2];

		stationElement3.visible = false;
		stationElement4.visible = false;
		stationElement5.visible = false;
	}

	function showNoordBrabant() {
		stationElement0.stationId = 6350;
		stationElement0.stationName = "Breda";
		stationElement0.city.anchors.topMargin = 42
		stationElement0.city.anchors.leftMargin = 100
		stationElement0.button.anchors.topMargin = -17
		stationElement0.button.anchors.leftMargin = 17
		stationElement0.button.bottomClickMargin = 10;
		stationElement0.button.topClickMargin = 10;
		stationElement0.button.text = qsTr("Breda");
		stationElement0.visible = true;
		linkElement0.width = 18;
		linkElement0.x = stationElement0.city.x - linkElement0.width + 1;
		linkElement0.y = stationElement0.city.y + 7;
		linkElement0.setCorrection(-1, 0, 1, 0, -1, 0, 1, 0);
		linkElement0.visible = true;
		p.drawDiagonalLine(linkElement1, stationElement0.city.x - 18 + 1, stationElement0.city.y + 7, p.vSpace(stationElement0.button, linkElement0) + 1, p.diagonalDirection.leftUp);
		linkElement1.setCorrection(0, 0, 1, 0, 0, 0, -1, 0);
		stationElement0.connectedLinks = [linkElement0, linkElement1];

		stationElement1.stationId = 6340;
		stationElement1.stationName = "Woensdrecht";
		stationElement1.city.anchors.topMargin = 83
		stationElement1.city.anchors.leftMargin = 16
		stationElement1.button.anchors.topMargin = 116
		stationElement1.button.anchors.leftMargin = 0
		stationElement1.button.text = qsTr("Woensdrecht");
		stationElement1.button.topClickMargin = 10;
		stationElement1.button.bottomClickMargin = 10;
		stationElement1.visible = true;
		linkElement2.height = 33 - stationElement1.city.height;
		linkElement2.width = 2;
		linkElement2.x = stationElement1.city.x + 7;
		linkElement2.y = stationElement1.city.y + 15;
		linkElement2.visible = true;
		linkElement2.shadowPos = linkElement2.shRight;
		linkElement2.setVerticalCorrection();
		stationElement1.connectedLinks = [linkElement2];

		stationElement2.stationId = 6375;
		stationElement2.stationName = "Uden";
		stationElement2.city.anchors.topMargin = 20
		stationElement2.city.anchors.leftMargin = 185
		stationElement2.button.anchors.topMargin = stationElement2.city.anchors.topMargin - 7
		stationElement2.button.anchors.leftMargin = 250
		stationElement2.button.topClickMargin = 10;
		stationElement2.button.bottomClickMargin = 10;
		stationElement2.button.text = qsTr("Uden");
		stationElement2.visible = true;
		linkElement3.width = stationElement2.button.anchors.leftMargin - stationElement2.city.anchors.leftMargin - stationElement2.city.width
		linkElement3.x = stationElement2.city.x + 15;
		linkElement3.y = stationElement2.city.y + 7;
		linkElement3.visible = true;
		stationElement2.connectedLinks = [linkElement3];

		stationElement3.stationId = 6370;
		stationElement3.stationName = "Eindhoven";
		stationElement3.city.anchors.topMargin = 81
		stationElement3.city.anchors.leftMargin = 170
		stationElement3.button.anchors.topMargin = stationElement3.city.anchors.topMargin - 7
		stationElement3.button.anchors.leftMargin = stationElement2.button.anchors.leftMargin
		stationElement3.button.topClickMargin = 10;
		stationElement3.button.text = qsTr("Eindhoven");
		stationElement3.button.topClickMargin = 10;
		stationElement3.button.bottomClickMargin = 10;
		stationElement3.visible = true;
		linkElement4.width = stationElement3.button.anchors.leftMargin - stationElement3.city.anchors.leftMargin - stationElement3.city.width
		linkElement4.x = stationElement3.city.x + 15;
		linkElement4.y = stationElement3.city.y + 7;
		linkElement4.visible = true;
		stationElement3.connectedLinks = [linkElement4];

		stationElement4.visible = false;
		stationElement5.visible = false;
	}

	function showGroningen() {
		stationElement0.stationId = 6277;
		stationElement0.stationName = "Groningen_Noord";
		stationElement0.city.anchors.topMargin = 14
		stationElement0.city.anchors.leftMargin = 12
		stationElement0.button.anchors.topMargin = stationElement0.city.anchors.topMargin - 11
		stationElement0.button.anchors.leftMargin = 186
		stationElement0.button.topClickMargin = 10;
		stationElement0.button.bottomClickMargin = 10;
		stationElement0.button.text = qsTr("Groningen Noord");
		stationElement0.visible = true;
		linkElement0.width = stationElement0.button.anchors.leftMargin - stationElement0.city.anchors.leftMargin - stationElement0.city.width
		linkElement0.x = stationElement0.city.x + 15;
		linkElement0.y = stationElement0.city.y + 7;
		linkElement0.visible = true;
		stationElement0.connectedLinks = [linkElement0];

		stationElement1.stationId = 6280;
		stationElement1.stationName = "Groningen";
		stationElement1.city.anchors.topMargin = 57
		stationElement1.city.anchors.leftMargin = 48
		stationElement1.button.anchors.topMargin = stationElement1.city.anchors.topMargin - 11
		stationElement1.button.anchors.leftMargin = -57
		stationElement1.button.topClickMargin = 10;
		stationElement1.button.bottomClickMargin = 10;
		stationElement1.button.text = qsTr("Groningen");
		stationElement1.visible = true;
		linkElement1.x = stationElement1.button.x + stationElement1.button.width;
		linkElement1.y = stationElement1.city.y + 7;
		linkElement1.width = stationElement1.city.x - linkElement1.x;
		linkElement1.setCorrection(0, 0, 1, 0, 0, 0, 0, 0);
		linkElement1.visible = true;
		stationElement1.connectedLinks = [linkElement1];

		stationElement2.stationId = 6286;
		stationElement2.stationName = "Groningen_Oost";
		stationElement2.city.anchors.topMargin = 62
		stationElement2.city.anchors.leftMargin = 135
		stationElement2.button.anchors.topMargin = 110
		stationElement2.button.anchors.leftMargin = stationElement0.button.anchors.leftMargin
		stationElement2.button.topClickMargin = 10;
		stationElement2.button.bottomClickMargin = 10;
		stationElement2.button.text = qsTr("Groningen Oost");
		stationElement2.visible = true;
		linkElement2.width = 2;
		linkElement2.height = 23 - stationElement2.city.height;
		linkElement2.x = stationElement2.city.x + 7;
		linkElement2.y = stationElement2.city.y + 15;
		linkElement2.shadowPos = linkElement2.shLeft;
		linkElement2.setCorrection(0, -1, 0, 3, 0, -1, 0, -1);
		linkElement2.visible = true;
		p.drawDiagonalLine(linkElement3, stationElement2.city.x + 7 + 1, stationElement2.city.y + 23, 44, p.diagonalDirection.rightDown);
		linkElement3.setCorrection(0, 0, 0, 0, 2, 0, -2, 0);
		stationElement2.connectedLinks = [linkElement2, linkElement3];

		stationElement4.visible = false;
		stationElement5.visible = false;
	}

	function showZeeland(province) {
		/* Removed from data set
		stationElement0.stationId = 6323;
		stationElement0.stationName = "Goes";
		stationElement0.city.anchors.topMargin = -(166+9+33+7+7);
		stationElement0.city.anchors.leftMargin = -(147+29+112);
		stationElement0.button.anchors.top = stationElement0.bottom
		stationElement0.button.anchors.topMargin = -(166+9+33+7+7+38);
		stationElement0.button.anchors.leftMargin = -147;
		stationElement0.button.text = qsTr("Goes");
		stationElement0.visible = true;
		linkElement0.width = 112 - stationElement0.city.width;
		linkElement0.x = stationElement0.city.x + 15;
		linkElement0.y = stationElement0.city.y + 7;
		linkElement0.visible = true;
		p.drawDiagonalLine(linkElement1, linkElement0.x + linkElement0.width - 1, linkElement0.y, 29, p.diagonalDirection.rightUp);
		*/

		stationElement1.stationId = 6310;
		stationElement1.stationName = "Vlissingen";
		linkElement2.width = 200
		stationElement1.city.anchors.topMargin = 76
		stationElement1.city.anchors.leftMargin = 23
		stationElement1.button.anchors.topMargin = stationElement1.city.anchors.topMargin - 11
		stationElement1.button.anchors.leftMargin = stationElement1.city.anchors.leftMargin + stationElement1.city.width + linkElement2.width
		stationElement1.button.text = qsTr("Vlissingen");
		stationElement1.button.bottomClickMargin = 2;
		stationElement1.button.topClickMargin = 10;
		stationElement1.visible = true;
		linkElement2.x = stationElement1.city.x + 15;
		linkElement2.y = stationElement1.city.y + 7;
		linkElement2.visible = true;
		stationElement1.connectedLinks = [linkElement2];

		stationElement2.stationId = 6319;
		stationElement2.stationName = "Terneuzen";
		stationElement2.city.anchors.topMargin = 118
		stationElement2.city.anchors.leftMargin = 78
		stationElement2.button.anchors.topMargin = stationElement2.city.anchors.topMargin - 11
		stationElement2.button.anchors.leftMargin = stationElement1.button.anchors.leftMargin
		stationElement2.button.text = qsTr("Terneuzen");
		stationElement2.button.bottomClickMargin = 10;
		stationElement2.button.topClickMargin = 3;
		stationElement2.visible = true;
		linkElement3.width = linkElement2.width + (stationElement1.city.anchors.leftMargin - stationElement2.city.anchors.leftMargin)
		linkElement3.x = stationElement2.city.x + 15;
		linkElement3.y = stationElement2.city.y + 7;
		linkElement3.visible = true;
		stationElement2.connectedLinks = [linkElement3];

		stationElement3.visible = false;
		stationElement4.visible = false;
		stationElement5.visible = false;
	}

	function showLimburg() {
		stationElement0.stationId = 6391;
		stationElement0.stationName = "Venlo";
		stationElement0.city.anchors.topMargin = 49
		stationElement0.city.anchors.leftMargin = 40
		stationElement0.button.anchors.topMargin = stationElement0.city.anchors.topMargin - 11
		stationElement0.button.anchors.leftMargin = 200
		stationElement0.button.bottomClickMargin = 2;
		stationElement0.button.topClickMargin = 10;
		stationElement0.button.text = qsTr("Venlo");
		stationElement0.visible = true;
		linkElement0.width = stationElement0.button.anchors.leftMargin - stationElement0.city.anchors.leftMargin - stationElement0.city.width
		linkElement0.x = stationElement0.city.x + 15;
		linkElement0.y = stationElement0.city.y + 7;
		linkElement0.visible = true;
		stationElement0.connectedLinks = [linkElement0];


		stationElement1.stationId = 6377;
		stationElement1.stationName = "Weert";
		stationElement1.city.anchors.topMargin = 88
		stationElement1.city.anchors.leftMargin = 8
		stationElement1.button.anchors.topMargin = stationElement1.city.anchors.topMargin - 11
		stationElement1.button.anchors.leftMargin = stationElement0.button.anchors.leftMargin;
		stationElement1.button.bottomClickMargin = 10;
		stationElement1.button.topClickMargin = 2;
		stationElement1.button.text = qsTr("Weert");
		stationElement1.visible = true;
		linkElement1.width = stationElement1.button.anchors.leftMargin - stationElement1.city.anchors.leftMargin - stationElement1.city.width
		linkElement1.x = stationElement1.city.x + 15;
		linkElement1.y = stationElement1.city.y + 7;
		linkElement1.visible = true;
		stationElement1.connectedLinks = [linkElement1];

		stationElement2.stationId = 6380;
		stationElement2.stationName = "Maastricht";
		stationElement2.city.anchors.topMargin = 163
		stationElement2.city.anchors.leftMargin = 6
		stationElement2.button.anchors.topMargin = stationElement2.city.anchors.topMargin - 11
		stationElement2.button.anchors.leftMargin = stationElement0.button.anchors.leftMargin;
		stationElement2.button.bottomClickMargin = 10;
		stationElement2.button.topClickMargin = 10;
		stationElement2.button.text = qsTr("Maastricht");
		stationElement2.visible = true;
		linkElement2.width = stationElement2.button.anchors.leftMargin - stationElement2.city.anchors.leftMargin - stationElement2.city.width
		linkElement2.x = stationElement2.city.x + 15;
		linkElement2.y = stationElement2.city.y + 7;
		linkElement2.visible = true;
		stationElement2.connectedLinks = [linkElement2];

		stationElement3.visible = false;
		stationElement4.visible = false;
		stationElement5.visible = false;
	}

	function showZuidHolland() {
		stationElement0.stationId = 6215;
		stationElement0.stationName = "Voorschoten";
		stationElement0.city.anchors.topMargin = 60
		stationElement0.city.anchors.leftMargin = 75
		stationElement0.button.anchors.topMargin = stationElement0.city.anchors.topMargin - 11
		stationElement0.button.anchors.leftMargin = 207
		stationElement0.button.bottomClickMargin = 9;
		stationElement0.button.topClickMargin = 10;
		stationElement0.button.text = qsTr("Voorschoten");
		stationElement0.visible = true;
		linkElement0.width = stationElement0.button.anchors.leftMargin - stationElement0.city.anchors.leftMargin - stationElement0.city.width
		linkElement0.x = stationElement0.city.x + 15;
		linkElement0.y = stationElement0.city.y + 7;
		linkElement0.visible = true;
		stationElement0.connectedLinks = [linkElement0];

		stationElement1.stationId = 6330;
		stationElement1.stationName = "Hoek_van_Holland";
		stationElement1.city.anchors.topMargin = 110
		stationElement1.city.anchors.leftMargin = 30
		stationElement1.button.anchors.topMargin = 10
		stationElement1.button.anchors.leftMargin = -73
		stationElement1.button.text = qsTr("Hoek van Holland");
		stationElement1.button.bottomClickMargin = 5;
		stationElement1.button.topClickMargin = 10;
		stationElement1.visible = true;
		linkElement1.width = 2;
		linkElement1.height = stationElement1.city.y - (stationElement1.button.y + stationElement1.button.height);
		linkElement1.y = (stationElement1.button.y + stationElement1.button.height);
		linkElement1.x = stationElement1.city.x + 7;
		linkElement1.setCorrection(0, 0, 0, 1, 0, 0, 0, 0);
		linkElement1.shadowPos = linkElement1.shRight;
		linkElement1.visible = true;
		stationElement1.connectedLinks = [linkElement1];

		stationElement2.stationId = 6356;
		stationElement2.stationName = "Gorinchem";
		stationElement2.city.anchors.topMargin = 118
		stationElement2.city.anchors.leftMargin = 156
		stationElement2.button.anchors.topMargin = stationElement2.city.anchors.topMargin - 11
		stationElement2.button.anchors.leftMargin = stationElement0.button.anchors.leftMargin
		stationElement2.button.bottomClickMargin = 10;
		stationElement2.button.topClickMargin = 10;
		stationElement2.button.text = qsTr("Gorinchem");
		stationElement2.visible = true;
		linkElement2.width = stationElement2.button.anchors.leftMargin - stationElement2.city.anchors.leftMargin - stationElement2.city.width
		linkElement2.x = stationElement2.city.x + 15;
		linkElement2.y = stationElement2.city.y + 7;
		linkElement2.visible = true;
		stationElement2.connectedLinks = [linkElement2];

		stationElement3.stationId = 6344;
		stationElement3.stationName = "Rotterdam";
		stationElement3.city.anchors.topMargin = 142
		stationElement3.city.anchors.leftMargin = 73
		stationElement3.button.anchors.topMargin = 54
		stationElement3.button.anchors.leftMargin = stationElement1.button.anchors.leftMargin
		stationElement3.button.bottomClickMargin = 10;
		stationElement3.button.topClickMargin = 5;
		stationElement3.button.text = qsTr("Rotterdam");
		stationElement3.visible = true;
		linkElement3.width = 40;
		linkElement3.x = stationElement3.city.x - linkElement3.width;
		linkElement3.y = stationElement3.city.y + 7;
		linkElement3.setCorrection(-1, 0, 2, 0, 0, 0, -1, 0);
		linkElement3.visible = true;
		p.drawDiagonalLine(linkElement4, stationElement3.city.x - 40 + 1, stationElement3.city.y + 7, p.vSpace(linkElement3, stationElement3.button) + 1, p.diagonalDirection.leftUp);
		linkElement4.clearCorrection();
		stationElement3.connectedLinks = [linkElement3, linkElement4];

		stationElement4.visible = false;
		stationElement5.visible = false;
	}

	function showNoordHolland() {
		stationElement0.stationId = 6235;
		stationElement0.stationName = "Den_Helder";
		stationElement0.city.anchors.topMargin = 40
		stationElement0.city.anchors.leftMargin = 16
		stationElement0.button.anchors.topMargin = stationElement0.city.anchors.topMargin - 11
		stationElement0.button.anchors.leftMargin = 185
		stationElement0.button.bottomClickMargin = 6;
		stationElement0.button.topClickMargin = 10;
		stationElement0.button.text = qsTr("Den Helder");
		stationElement0.visible = true;
		linkElement0.width = (3+29+137) - stationElement0.city.width;
		linkElement0.x = stationElement0.city.x + 15;
		linkElement0.y = stationElement0.city.y + 7;
		linkElement0.visible = true;
		stationElement0.connectedLinks = [linkElement0];


		stationElement1.stationId = 6249;
		stationElement1.stationName = "Berkhout";
		stationElement1.city.anchors.topMargin = 88
		stationElement1.city.anchors.leftMargin = 48
		stationElement1.button.anchors.topMargin = stationElement1.city.anchors.topMargin - 11
		stationElement1.button.anchors.leftMargin = stationElement0.button.anchors.leftMargin
		stationElement1.button.bottomClickMargin = 10;
		stationElement1.button.topClickMargin = 6;
		stationElement1.button.text = qsTr("Berkhout");
		stationElement1.visible = true;
		linkElement1.width = (137) - stationElement1.city.width;
		linkElement1.x = stationElement1.city.x + 15;
		linkElement1.y = stationElement1.city.y + 7;
		linkElement1.visible = true;
		stationElement1.connectedLinks = [linkElement1];

		stationElement2.stationId = 6240;
		stationElement2.stationName = "Amsterdam";
		stationElement2.city.anchors.topMargin = 161
		stationElement2.city.anchors.leftMargin = 19
		stationElement2.button.anchors.topMargin = stationElement2.city.anchors.topMargin - 11
		stationElement2.button.anchors.leftMargin = stationElement0.button.anchors.leftMargin
		stationElement2.button.bottomClickMargin = 10;
		stationElement2.button.topClickMargin = 10;
		stationElement2.button.text = qsTr("Amsterdam");
		stationElement2.visible = true;
		linkElement2.width = (29+137) - stationElement2.city.width;
		linkElement2.x = stationElement2.city.x + 15;
		linkElement2.y = stationElement2.city.y + 7;
		linkElement2.visible = true;
		stationElement2.connectedLinks = [linkElement2];

		stationElement3.visible = false;
		stationElement4.visible = false;
		stationElement5.visible = false;
	}

	function showFlevoland() {
		stationElement0.stationId = 6273;
		stationElement0.stationName = "Noordoostpolder";
		stationElement0.city.anchors.topMargin = 31
		stationElement0.city.anchors.leftMargin = 122
		stationElement0.button.anchors.topMargin = stationElement0.city.anchors.topMargin - 11
		stationElement0.button.anchors.leftMargin = 200
		stationElement0.button.bottomClickMargin = 10;
		stationElement0.button.topClickMargin = 10;
		stationElement0.button.text = qsTr("Noordoostpolder");
		stationElement0.visible = true;
		linkElement0.width = stationElement0.button.anchors.leftMargin - stationElement0.city.anchors.leftMargin - stationElement0.city.width
		linkElement0.x = stationElement0.city.x + 15;
		linkElement0.y = stationElement0.city.y + 7;
		linkElement0.visible = true;
		stationElement0.connectedLinks = [linkElement0];


		stationElement1.stationId = 6269;
		stationElement1.stationName = "Lelystad";
		stationElement1.city.anchors.topMargin = 96
		stationElement1.city.anchors.leftMargin = 66
		stationElement1.button.anchors.topMargin = stationElement1.city.anchors.topMargin - 11
		stationElement1.button.anchors.leftMargin = stationElement0.button.anchors.leftMargin
		stationElement1.button.bottomClickMargin = 10;
		stationElement1.button.topClickMargin = 10;
		stationElement1.button.text = qsTr("Lelystad");
		stationElement1.visible = true;
		linkElement1.width = stationElement1.button.anchors.leftMargin - stationElement1.city.anchors.leftMargin - stationElement1.city.width
		linkElement1.x = stationElement1.city.x + 15;
		linkElement1.y = stationElement1.city.y + 7;
		linkElement1.visible = true;
		stationElement1.connectedLinks = [linkElement1];

		stationElement2.visible = false;
		stationElement3.visible = false;
		stationElement4.visible = false;
		stationElement5.visible = false;
	}

	function showOverijssel() {
		stationElement0.stationId = 6278;
		stationElement0.stationName = "Zwolle";
		stationElement0.city.anchors.topMargin = 72
		stationElement0.city.anchors.leftMargin = 39
		stationElement0.button.anchors.topMargin = stationElement0.city.anchors.topMargin - 11
		stationElement0.button.anchors.leftMargin = 239
		stationElement0.button.bottomClickMargin = 10;
		stationElement0.button.topClickMargin = 10;
		stationElement0.button.text = qsTr("Zwolle");
		stationElement0.visible = true;
		linkElement0.width = stationElement0.button.anchors.leftMargin - stationElement0.city.anchors.leftMargin - stationElement0.city.width
		linkElement0.x = stationElement0.city.x + 15;
		linkElement0.y = stationElement0.city.y + 7;
		linkElement0.visible = true;
		stationElement0.connectedLinks = [linkElement0];

		stationElement1.stationId = 6290;
		stationElement1.stationName = "Twente";
		stationElement1.city.anchors.topMargin = 142
		stationElement1.city.anchors.leftMargin = 145
		stationElement1.button.anchors.topMargin = stationElement1.city.anchors.topMargin - 11
		stationElement1.button.anchors.leftMargin = stationElement0.button.anchors.leftMargin
		stationElement1.button.bottomClickMargin = 10;
		stationElement1.button.topClickMargin = 10;
		stationElement1.button.text = qsTr("Twente");
		stationElement1.visible = true;
		linkElement1.width = stationElement1.button.anchors.leftMargin - stationElement1.city.anchors.leftMargin - stationElement1.city.width
		linkElement1.x = stationElement1.city.x + 15;
		linkElement1.y = stationElement1.city.y + 7;
		linkElement1.visible = true;
		stationElement1.connectedLinks = [linkElement1];

		stationElement2.visible = false;
		stationElement3.visible = false;
		stationElement4.visible = false;
		stationElement5.visible = false;
	}

	function showDrenthe() {
		stationElement0.stationId = 6279;
		stationElement0.stationName = "Hoogeveen";
		stationElement0.city.anchors.topMargin = 131
		stationElement0.city.anchors.leftMargin = 58
		stationElement0.button.anchors.topMargin = stationElement0.city.anchors.topMargin - 11
		stationElement0.button.anchors.leftMargin = 247
		stationElement0.button.bottomClickMargin = 10;
		stationElement0.button.topClickMargin = 10;
		stationElement0.button.text = qsTr("Hoogeveen");
		stationElement0.visible = true;
		linkElement0.width = stationElement0.button.anchors.leftMargin - stationElement0.city.anchors.leftMargin - stationElement0.city.width
		linkElement0.x = stationElement0.city.x + 15;
		linkElement0.y = stationElement0.city.y + 7;
		linkElement0.visible = true;
		stationElement0.connectedLinks = [linkElement0];

		stationElement1.visible = false;
		stationElement2.visible = false;
		stationElement3.visible = false;
		stationElement4.visible = false;
		stationElement5.visible = false;
	}

	function showFriesland() {
		stationElement0.stationId = 6251;
		stationElement0.stationName = "Terschelling";
		stationElement0.city.anchors.topMargin = 22
		stationElement0.city.anchors.leftMargin = 45
		stationElement0.button.anchors.topMargin = stationElement0.city.anchors.topMargin - 11
		stationElement0.button.anchors.leftMargin = 220
		stationElement0.button.bottomClickMargin = 5;
		stationElement0.button.topClickMargin = 10;
		stationElement0.button.text = qsTr("Terschelling");
		stationElement0.visible = true;
		linkElement0.width = stationElement0.button.anchors.leftMargin - stationElement0.city.anchors.leftMargin - stationElement0.city.width
		linkElement0.x = stationElement0.city.x + 15;
		linkElement0.y = stationElement0.city.y + 7;
		linkElement0.visible = true;
		stationElement0.connectedLinks = [linkElement0];

		stationElement1.stationId = 6242;
		stationElement1.stationName = "Vlieland";
		stationElement1.city.anchors.topMargin = 48
		stationElement1.city.anchors.leftMargin = 10
		stationElement1.button.anchors.topMargin = 122
		stationElement1.button.anchors.leftMargin = -21
		stationElement1.button.bottomClickMargin = 10;
		stationElement1.button.topClickMargin = 10;
		stationElement1.button.text = qsTr("Vlieland");
		stationElement1.visible = true;
		linkElement1.height = stationElement1.button.y - (stationElement1.city.y + stationElement1.city.height);
		linkElement1.width = 2;
		linkElement1.x = stationElement1.city.x + 7;
		linkElement1.y = stationElement1.city.y + 15;
		linkElement1.setVerticalCorrection();
		linkElement1.shadowPos = linkElement1.shRight;
		linkElement1.visible = true;
		stationElement1.connectedLinks = [linkElement1];

		stationElement2.stationId = 6270;
		stationElement2.stationName = "Leeuwarden";
		stationElement2.city.anchors.topMargin = 52
		stationElement2.city.anchors.leftMargin = 112
		stationElement2.button.anchors.topMargin = 59
		stationElement2.button.anchors.leftMargin = stationElement0.button.anchors.leftMargin
		stationElement2.button.text = qsTr("Leeuwarden");
		stationElement2.visible = true;
		stationElement2.button.topClickMargin = 5;
		stationElement2.button.bottomClickMargin = 8;
		linkElement2.width = (90) - stationElement2.city.width;
		linkElement2.x = stationElement2.city.x + 15;
		linkElement2.y = stationElement2.city.y + 7;
		linkElement2.setCorrection(-1, 0, 1, 0, -1, 0, 0, 0);
		linkElement2.visible = true;
		p.drawDiagonalLine(linkElement3, linkElement2.x + linkElement2.width, linkElement2.y, 17, p.diagonalDirection.rightDown);
		linkElement3.setCorrection(1, 0, 0, 0, 0, 0, 0, 0);
		stationElement2.connectedLinks = [linkElement2, linkElement3];

		stationElement3.stationId = 6267;
		stationElement3.stationName = "Friesland_West";
		stationElement3.city.anchors.topMargin = 122
		stationElement3.city.anchors.leftMargin = 68
		stationElement3.button.anchors.topMargin = stationElement3.city.anchors.topMargin - 11
		stationElement3.button.anchors.leftMargin = stationElement0.button.anchors.leftMargin
		stationElement3.button.bottomClickMargin = 10;
		stationElement3.button.topClickMargin = 10;
		stationElement3.button.text = qsTr("Friesland West");
		stationElement3.visible = true;
		linkElement4.width = stationElement0.button.anchors.leftMargin - stationElement0.city.anchors.leftMargin - stationElement0.city.width
		linkElement4.x = stationElement3.city.x + 15;
		linkElement4.y = stationElement3.city.y + 7;
		linkElement4.visible = true;
		stationElement3.connectedLinks = [linkElement4];

		stationElement4.visible = false;
		stationElement5.visible = false;
	}

	function showUtrecht() {
		provinceImg.anchors.leftMargin = -35;

		stationElement0.stationId = 6260;
		stationElement0.stationName = "Utrecht";
		stationElement0.city.anchors.topMargin = 84
		stationElement0.city.anchors.leftMargin = 100
		stationElement0.button.anchors.topMargin = stationElement0.city.anchors.topMargin - 11
		stationElement0.button.anchors.leftMargin = 263
		stationElement0.button.bottomClickMargin = 10;
		stationElement0.button.topClickMargin = 10;
		stationElement0.button.text = qsTr("Utrecht");
		stationElement0.visible = true;
		linkElement0.width = stationElement0.button.anchors.leftMargin - stationElement0.city.anchors.leftMargin - stationElement0.city.width
		linkElement0.x = stationElement0.city.x + 15;
		linkElement0.y = stationElement0.city.y + 7;
		linkElement0.visible = true;
		stationElement0.connectedLinks = [linkElement0];

		stationElement1.stationId = 6348;
		stationElement1.stationName = "Utrecht_West";
		stationElement1.city.anchors.topMargin = 156
		stationElement1.city.anchors.leftMargin = 28
		stationElement1.button.anchors.topMargin = stationElement1.city.anchors.topMargin - 11
		stationElement1.button.anchors.leftMargin = stationElement0.button.anchors.leftMargin
		stationElement1.button.bottomClickMargin = 10;
		stationElement1.button.topClickMargin = 10;
		stationElement1.button.text = qsTr("Utrecht-West");
		stationElement1.visible = true;
		linkElement1.width = stationElement1.button.anchors.leftMargin - stationElement1.city.anchors.leftMargin - stationElement1.city.width
		linkElement1.x = stationElement1.city.x + 15;
		linkElement1.y = stationElement1.city.y + 7;
		linkElement1.visible = true;
		stationElement1.connectedLinks = [linkElement1];

		stationElement2.visible = false;
		stationElement3.visible = false;
		stationElement4.visible = false;
		stationElement5.visible = false;
	}

	Image {
		id: provinceImg
		anchors.top: parent.top
		anchors.left: parent.left

		anchors.topMargin: isNxt ? 122 : 60

		LinkElement {id: linkElement0}
		LinkElement {id: linkElement1}
		LinkElement {id: linkElement2}
		LinkElement {id: linkElement3}
		LinkElement {id: linkElement4}
		LinkElement {id: linkElement5}
		LinkElement {id: linkElement6}
		LinkElement {id: linkElement7}
		LinkElement {id: linkElement8}
		LinkElement {id: linkElement9}
	}

	WeatherStationElement {
		id: stationElement0
		//width: parent.width
		//height: parent.height
		anchors.fill: provinceImg
		onStationClicked: p.stationSelected(0);
	}
	WeatherStationElement {
		id: stationElement1
		//width: parent.width
		//height: parent.height
		anchors.fill: provinceImg
		onStationClicked: p.stationSelected(1);
	}
	WeatherStationElement {
		id: stationElement2
		//width: parent.width
		//height: parent.height
		anchors.fill: provinceImg
		onStationClicked: p.stationSelected(2);
	}
	WeatherStationElement {
		id: stationElement3
		//width: parent.width
		//height: parent.height
		anchors.fill: provinceImg
		onStationClicked: p.stationSelected(3);
	}
	WeatherStationElement {
		id: stationElement4
		//width: parent.width
		//height: parent.height
		anchors.fill: provinceImg
		onStationClicked: p.stationSelected(4);
	}
	WeatherStationElement {
		id: stationElement5
		//width: parent.width
		//height: parent.height
		anchors.fill: provinceImg
		onStationClicked: p.stationSelected(5);
	}

}
