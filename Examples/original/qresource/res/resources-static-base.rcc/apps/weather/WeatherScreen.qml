import QtQuick 2.1
import qb.components 1.0
import "weather.js" as WeatherJS;

Screen {
	id: weatherScreen
	screenTitleIconUrl: "drawables/weather.svg"
	screenTitle: qsTr("Weather")

	property int feelsLikeTemperature: 0
	property real actualTemperature: 0
	property bool detailsButtonClicked: false
	property int currentDay: 0

	onHidden: {		
		app.weatherDataReceived.disconnect(updateData);
		app.getWeatherUpdate(app.weatherUpdatePartial);
	}

	onShown: {
		initiate();
	}

	onCustomButtonClicked: {
		stage.openFullscreen(app.weatherStationScreenUrl);
	}

	function makeBigImageChoice() {
		if (app) {
			bigImage.source = WeatherJS.imageOrCross(!app.weatherDataRead, true, "image://scaled/apps/weather/drawables/Home",
									   app.weatherStationData ['iconID'],
									   app.weatherStationData ['iconText']);
		}
	}

	function makeTinyImageChoice() {
		if (app) {
			for (var i = 0; i < 6; i++) {
				var temp = app.weatherForecastData[i];
				forecastColumn.children[i].tinyIconSource =
						WeatherJS.imageOrCross(!app.weatherDataRead, (i > 0), "image://scaled/apps/weather/drawables/Home", temp ? temp['iconID'] : "", "");
			}
		}
	}

	function putForecastTemperatures() {
		for (var i = 0; i < 6; i++) {
			var temp = app.weatherForecastData[i];
			forecastColumn.children[i].dayTemperatureText =
					WeatherJS.processTemperatureValue(!app.weatherDataRead, temp ? temp['maxTemperature'] : "", 0, false);
			forecastColumn.children[i].nightTemperatureText =
					WeatherJS.processTemperatureValue(!app.weatherDataRead, temp ? temp['minTemperature'] : "", 0, false);
		}
	}

	function updateData() {
		makeBigImageChoice();
		makeTinyImageChoice();
		putForecastTemperatures();
	}

	function initiate() {
		var d = new Date();
		currentDay = d.getDay();
		addCustomTopRightButton(qsTr("Weatherstation"));

		//If we get an data failure or change for success to failure update shown data
		app.weatherDataReceived.connect(updateData);
		//Request full info here, will return to partial when hidden (so also when details or weatherstate screen is opened)
		app.getWeatherUpdate(app.weatherUpdateFull);
	}

	Row {
		id: row
		anchors {
			top: parent.top
			topMargin: Math.round(45 * verticalScaling)
			horizontalCenter: parent.horizontalCenter
		}
		spacing: designElements.hMargin20

		Item {
			id: siteColumn
			width: Math.round(405 * horizontalScaling)
			height: childrenRect.height

			Text {
				id: placeText
				font {
					pixelSize: qfont.navigationTitle
					family: qfont.semiBold.name
				}
				color: colors.waSystemState
				text: app.location
			}

			RoundedRectangle {
				id: mapRectangle
				width: parent.width
				anchors {
					top: placeText.bottom
					topMargin: designElements.vMargin10
				}
				height: Math.round(256 * verticalScaling)
				radius: designElements.radius
				color: colors.contentBackground

				RadarImage {
					id: radarImage
					anchors {
						top: parent.top
						topMargin: Math.round(8 * verticalScaling)
						left: parent.left
						leftMargin: anchors.topMargin
					}
					size: Math.round(240 * verticalScaling)

					onClicked: stage.openFullscreen(app.weatherDetailsScreenUrl)
				}

				Item {
					anchors {
						top: radarImage.top
						topMargin: designElements.vMargin10
						left: radarImage.right
						right: parent.right
						bottom: radarImage.bottom
					}

					Image {
						id: bigImage
						anchors {
							horizontalCenter: parent.horizontalCenter
							top: parent.top
						}
					}

					Text {
						id: bigTemperatureText
						anchors {
							top: bigImage.bottom
							topMargin: designElements.vMargin10
							horizontalCenter: parent.horizontalCenter
						}
						font {
							pixelSize: qfont.spinnerText
							family: qfont.regular.name
						}
						color: colors.menuBarLabelDown
						text: WeatherJS.processTemperatureValue(!app.weatherDataRead, app.weatherStationData['temperatuurGC'], 1, true);
					}

					Text {
						id: feelsLikeText
						anchors {
							bottom: feelsLikeTemp.top
							bottomMargin: designElements.vMargin5
							left: parent.left
							leftMargin: designElements.hMargin10
							right: parent.right
							rightMargin: anchors.leftMargin
						}
						font {
							pixelSize: qfont.tileTitle
							family: qfont.regular.name
						}
						color: colors.menuBarLabelDown
						horizontalAlignment: Text.AlignHCenter
						wrapMode: Text.WordWrap
						text: qsTr ("Feels like")
					}

					Text {
						id: feelsLikeTemp
						anchors {
							baseline: parent.bottom
							baselineOffset: - designElements.vMargin20
							horizontalCenter: parent.horizontalCenter
						}
						font {
							pixelSize: qfont.secondaryImportantBodyText
							family: qfont.regular.name
						}
						color: colors.menuBarLabelDown
						text: WeatherJS.processTemperatureValue(!app.weatherDataRead, app.getWindChill(), 1, true)
					}
				}
			}

			StandardButton {
				id: detailsStandardButton
				anchors {
					top: mapRectangle.bottom
					topMargin: designElements.vMargin10
					right: parent.right
				}
				text: qsTr("Details")

				onClicked: stage.openFullscreen(app.weatherDetailsScreenUrl)
			}
		}

		Column {
			spacing: designElements.vMargin10
			
			Text {
				id: forecastText
				font {
					pixelSize: qfont.navigationTitle
					family: qfont.semiBold.name
				}
				color: colors.waSystemState
				text: qsTr("National Forecast")
			}

			Column {
				id: forecastColumn
				spacing: designElements.spacing8

				Repeater {
					id: daysRepeater
					model: 6
					DayRectangle {
						dayIndex: currentDay + index
						Component.onCompleted: {
							if (index === 0) {
								dayTextFontFamily = qfont.semiBold.name;
								dayTextColor = colors.waTodayForecastTextColor;
							}
						}
					}
				}
			}
		}
	}

	Text {
		id: srcText
		text: qsTr("Source: %1").arg("Buienradar.nl")
		anchors {
			baseline: parent.bottom
			baselineOffset: - anchors.rightMargin
			right: parent.right
			rightMargin: designElements.hMargin5
		}
		font {
			pixelSize: qfont.bodyText
			family: qfont.italic.name
		}
		color: colors.waSystemState
	}
}
