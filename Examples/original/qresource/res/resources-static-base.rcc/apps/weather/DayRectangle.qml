import QtQuick 2.1
import qb.components 1.0

Rectangle {
	radius: designElements.radius
	height: Math.round(36 * verticalScaling)
	width: Math.round(278 * horizontalScaling)
	color: colors.contentBackground
	property int dayIndex: -1
	property alias dayTextFontFamily: dayText.font.family
	property alias dayTextColor: dayText.color
	property alias tinyIconSource: tinyWeatherIcon.source
	property alias dayTemperatureText: dayTemperatureText.text
	property alias nightTemperatureText: nightTemperatureText.text

	Text {
		id: dayText
		font {
			pixelSize: qfont.bodyText
			family: qfont.regular.name
			capitalization: Font.Capitalize
		}
		color: colors.waBody01up
		anchors {
			left: parent.left
			leftMargin: Math.round(8 * horizontalScaling)
			verticalCenter: parent.verticalCenter
		}
		text: dayIndex == currentDay ? qsTr("Today") : i18n.daysFull[dayIndex % 7];
	}

	Image {
		id: tinyWeatherIcon
		anchors {
			left: parent.left
			leftMargin: Math.round(146 * horizontalScaling)
			verticalCenter: parent.verticalCenter
		}
		sourceSize.height: Math.round(24 * verticalScaling)
	}

	Text {
		id: dayTemperatureText
		property int temperatureDegrees
		anchors {
			left: tinyWeatherIcon.right
			leftMargin: Math.round(12 * horizontalScaling)
			verticalCenter: parent.verticalCenter
		}
		font {
			pixelSize: qfont.bodyText
			family: qfont.regular.name
		}
		color: colors.waBody01up
	}

	Image {
		id: blackMoonImage
		anchors {
			left: tinyWeatherIcon.right
			leftMargin: Math.round(46 * horizontalScaling)
			verticalCenter: parent.verticalCenter
		}
		source: "image://scaled/apps/weather/drawables/BlackMoon.svg"
		sourceSize.height: Math.round(24 * verticalScaling)
	}

	Text {
		id: nightTemperatureText
		anchors {
			left: blackMoonImage.right
			leftMargin: designElements.hMargin6
			verticalCenter: parent.verticalCenter
		}
		font {
			pixelSize: qfont.bodyText
			family: qfont.regular.name
		}
		color: colors.waBody01up
	}
}
