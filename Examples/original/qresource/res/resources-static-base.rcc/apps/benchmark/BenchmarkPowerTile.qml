import QtQuick 2.1

BenchmarkTile {
	headTextContent: qsTr("Benchmark power yesterday")
	type: "elec"
	unit: "kWh"
}
