import QtQuick 2.1
import qb.components 1.0

RevenueTile {
	displayMoneyWise: true
	value: app.totalProducedMoney
}
