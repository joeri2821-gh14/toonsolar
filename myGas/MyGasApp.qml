import QtQuick 2.1
import qb.components 1.0
import qb.base 1.0


/*
 * MyGasApp.qml
 *
 * Application that displays Gas usage on a tile.
 */
App {
    id: myGasApp
    
    property url tileUrl : "MyGasTile.qml"
    property url thumbnailIcon: "image://apps/graph/drawables/ChooseTileMoment.svg"
    property double gasM3: NaN
    property string serverGasPath: "/toon/gas"
    property string serverUrl: "http://10.10.0.125:8082"
    
    function init() {
        console.log("MyGasApp: init")
        registry.registerWidget( "tile", tileUrl, this, null, 
         { thumbLabel: qsTr("Gas usage"), 
           thumbIcon: thumbnailIcon, 
           thumbCategory: "general", 
           thumbWeight: 30, 
           baseTileWeight: 10, 
           thumbIconVAlignment: "center" } );
	}
	
	function update() {
		console.log("MyGasApp: start update")
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					var json = JSON.parse( xmlhttp.responseText );
					gasM3 = json.usage.today;
				} else {
					// Something went wrong, set counters to NaN
					gasM3 = NaN;
					console.log("MyGasApp: failed to collect Gas usage readings, server error code " + xmlhttp.status + ", message: " + xmlhttp.statusText)
				}
			}
		}
		xmlhttp.open("GET", serverUrl + serverGasPath, true);
		xmlhttp.send();
	}
	
	Timer {
		id: gasUpdateTimer
		interval: 60000  // update every minute
		triggeredOnStart: true
		running: true
		repeat: true
		onTriggered: update()
	}
}
