import QtQuick 2.1
import qb.components 1.0

Tile {
	id: myGasTile

	property variant arrayColors : [	colors.powerTileBar0,
						colors.powerTileBar1,
						colors.powerTileBar2,
						colors.powerTileBar3,
						colors.powerTileBar4,
						colors.powerTileBar5,
						colors.powerTileBar6,
						colors.powerTileBar7,
						colors.powerTileBar8,
						colors.powerTileBar9
						]
    
	property variant arrayDimmedColors : [	
						dimmableColors.powerTileBar0,
						dimmableColors.powerTileBar1,
						dimmableColors.powerTileBar2,
						dimmableColors.powerTileBar3,
						dimmableColors.powerTileBar4,
						dimmableColors.powerTileBar5,
						dimmableColors.powerTileBar6,
						dimmableColors.powerTileBar7,
						dimmableColors.powerTileBar8,
						dimmableColors.powerTileBar9
						]
	property string value : "-"
	property bool dimState: screenStateController.dimmedColors
    
	QtObject {
		id: p
		property int tileBars : 10
		function redraw() {
	                redrawUsage();
		}
        
        function redrawUsage() {
		console.log("MyGasApp redrawUsage(): " + app.gasM3);
		var pb = gasList.children;
		value = app.gasM3;
		var filledBars;
		if (isNaN(value)) {
			console.log("MyGasApp not set/not a number");
			filledBars = 0;
			gasValue.text = '-';
		} else {
			filledBars = 0;
			if (value > 6) {
			    filledBars = 10;
			} else if (value > 5) {
			    filledBars = 9;
			} else if (value > 4) {
				filledBars = 8;
			} else if (value > 3) {
				filledBars = 7;
			} else if (value > 2.5) {
				filledBars = 6;
			} else if (value > 2) {
				filledBars = 5;
			} else if (value > 1.5) {
				filledBars = 4;
			} else if (value > 1) {
				filledBars = 3;
			} else if (value > 0.5) {
				filledBars = 2;
			} else if (value > 0.1) {
				filledBars = 1;
			}
			gasValue.text = qsTr("%1 m\u00B3").arg(i18n.number( Number( value ), 2 ));
		}

		for (var i = 0; i < tileBars; i++) {
			if (!dimState) {
				pb[i].color =  (i < filledBars) ? arrayColors[i] : colors.powerTileBarEmpty;
        	        } else {
				pb[i].color =  (i < filledBars) ? arrayDimmedColors[i] : dimmableColors.powerTileBarEmpty;
			}
		}
	}
       } 

	function init() {
		app.gasM3Changed.connect(p.redraw);
		p.redraw();
	}

	Component.onDestruction: app.gasM3Changed.disconnect(p.redraw);
	onDimStateChanged: p.redraw()

	Text {
		id: gasWidgetText
		color: colors.tileTitleColor
		text: qsTr("Verbruik")
		anchors {
			baseline: parent.top
			baselineOffset: Math.round(30 * verticalScaling)
			horizontalCenter: parent.horizontalCenter
		}
		font.pixelSize: qfont.tileTitle
		font.family: qfont.regular.name
	       visible: !dimState
	}

	GasList {
		id: gasList
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.verticalCenter: parent.verticalCenter
	}

	Text {
		id: gasValue
		color: (typeof dimmableColors !== 'undefined') ? dimmableColors.clockTileColor : (typeof dimmableColors !== 'undefined') ? dimmableColors.clockTileColor : colors.clockTileColor
		anchors {
			horizontalCenter: parent.horizontalCenter
			baseline: parent.bottom
			baselineOffset: designElements.vMarginNeg16
		}
		font.pixelSize: qfont.tileText
		font.family: qfont.regular.name
	}
}
