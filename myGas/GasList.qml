import QtQuick 2.1

Item {
	id: gasBar
	width: childrenRect.width
	height: childrenRect.height

	Rectangle {
		id: gasBlock9
		x: 0
		y: Math.round(72 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: gasBlock8
		x: 0
		y: Math.round(64 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: gasBlock7
		x: 0
		y: Math.round(56 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: gasBlock6
		x: 0
		y: Math.round(48 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: gasBlock5
		x: 0
		y: Math.round(40 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: gasBlock4
		x: 0
		y: Math.round(32 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: gasBlock3
		x: 0
		y: Math.round(24 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: gasBlock2
		x: 0
		y: Math.round(16 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: gasBlock1
		x: 0
		y: Math.round(8 * verticalScaling)
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}

	Rectangle {
		id: gasBlock0
		x: 0
		y: 0
		width: Math.round(32 * horizontalScaling)
		height: Math.round(5 * verticalScaling)
		color: colors.powerTileBarEmpty
	}
}
