import QtQuick 2.1
import qb.components 1.0

Tile {
	id: myPowerTile

	property variant arrayColors : [colors.powerTileBar0,
									colors.powerTileBar1,
									colors.powerTileBar2,
									colors.powerTileBar3,
									colors.powerTileBar4,
									colors.powerTileBar5,
									colors.powerTileBar6,
									colors.powerTileBar7,
									colors.powerTileBar8,
									colors.powerTileBar9]
    
    property variant arrayDimmedColors : [  dimmableColors.powerTileBar0,
                                            dimmableColors.powerTileBar1,
                                            dimmableColors.powerTileBar2,
                                            dimmableColors.powerTileBar3,
                                            dimmableColors.powerTileBar4,
                                            dimmableColors.powerTileBar5,
                                            dimmableColors.powerTileBar6,
                                            dimmableColors.powerTileBar7,
                                            dimmableColors.powerTileBar8,
                                            dimmableColors.powerTileBar9]
    
	property string value : "-"
    property bool dimState: screenStateController.dimmedColors
    
	QtObject {
		id: p

		property int tileBars : 10

		function redraw() {
            if (app.powerW > 0) {
                redrawUsage();
            } else {
                redrawProduction();
            }
        }        
        
        function redrawUsage() {
            console.log("MyPowerApp redrawUsage(): " + app.powerW);
			var pb = powerList.children;
			var avg = 1500;
			value = app.powerW;
			var filledBars;
			if (isNaN(value) || isNaN(avg)) {
				filledBars = 0;
				powerValue.text = '-';
			} else {
				// filledBars = Math.round(value / (avg / 3));
				filledBars = 0;
				if (value > 8000) {
				    filledBars = 10;
				} else if (value > 15000) {
				    filledBars = 9;
				} else if (value > 12500) {
					filledBars = 8;
				} else if (value > 10000) {
					filledBars = 7;
				} else if (value > 7500) {
					filledBars = 6;
				} else if (value > 5000) {
					filledBars = 5;
				} else if (value > 2500) {
					filledBars = 4;
				} else if (value > 1000) {
					filledBars = 3;
				} else if (value > 500) {
					filledBars = 2;
				} else if (value > 100) {
					filledBars = 1;
				}
				powerValue.text = qsTr("%1 Watt").arg(value);
			}

			for (var i = 0; i < tileBars; i++) {
				if (!dimState) {
                    pb[i].color =  (i < filledBars) ? arrayColors[i] : colors.powerTileBarEmpty;
                } else {
                    pb[i].color =  (i < filledBars) ? arrayDimmedColors[i] : dimmableColors.powerTileBarEmpty;
                }
			}
		}
        
        function redrawProduction() {
            console.log("MyPowerApp redrawProduction(): " + app.powerW);
			var pb = powerList.children;
			var avg = 500;
			value = Math.abs(app.powerW);
			var filledBars;
			if (isNaN(value) || isNaN(avg)) {
				filledBars = 0;
				powerValue.text = '-';
			} else {
				filledBars = Math.round(value / (avg / 3));
				powerValue.text = qsTr("-%1 Watt").arg(value);
			}

			for (var i = 0; i < tileBars; i++) {
                if (!dimState) {
                    pb[tileBars - 1 - i].color = (i < filledBars) ? "#FFF950" : colors.powerTileBarEmpty;
                } else {
                    pb[tileBars - 1 - i].color = (i < filledBars) ? arrayDimmedColors[i] : dimmableColors.powerTileBarEmpty;
                }
			}
		}
	}

	function init() {
		if (app.powerUsageDataRead)
			p.redraw();
		app.powerWChanged.connect(p.redraw);
        
        console.log("Colors:" + JSON.stringify(colors));
	}

	Component.onDestruction: app.powerWChanged.disconnect(p.redraw);

	onDimStateChanged: p.redraw()

	Text {
		id: powerWidgetText
		color: colors.tileTitleColor
		text: qsTr("Vermogen in/uit")
		anchors {
			baseline: parent.top
			baselineOffset: Math.round(30 * verticalScaling)
			horizontalCenter: parent.horizontalCenter
		}
		font.pixelSize: qfont.tileTitle
		font.family: qfont.regular.name
        visible: !dimState
	}

	PowerList {
		id: powerList
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.verticalCenter: parent.verticalCenter
	}

	Text {
		id: powerValue
		color: (typeof dimmableColors !== 'undefined') ? dimmableColors.clockTileColor : (typeof dimmableColors !== 'undefined') ? dimmableColors.clockTileColor : colors.clockTileColor
		anchors {
			horizontalCenter: parent.horizontalCenter
			baseline: parent.bottom
			baselineOffset: designElements.vMarginNeg16
		}
		font.pixelSize: qfont.tileText
		font.family: qfont.regular.name
	}
}
