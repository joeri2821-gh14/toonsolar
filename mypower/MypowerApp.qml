import QtQuick 2.1
import qb.components 1.0
import qb.base 1.0


/*
 * MyPowerApp.qml
 *
 * Application that displays P1 in/out on a tile.
 */
App {
    id: mypowerApp
    
    property url tileUrl : "MyPowerTile.qml"
    property url thumbnailIcon: "image://apps/graph/drawables/ChooseTileMoment.svg"
    property int powerW: NaN
    property string serverP1Path: "/p1/latest"
    property string serverUrl: "http://10.10.0.125:8083"
    
    function init() {
        console.log("MyPowerApp: init")
        registry.registerWidget( "tile", tileUrl, this, null, 
         { thumbLabel: qsTr("P1 in/uit"), 
           thumbIcon: thumbnailIcon, 
           thumbCategory: "general", 
           thumbWeight: 30, 
           baseTileWeight: 10, 
           thumbIconVAlignment: "center" } );
    }
	
	function update() {
        console.log("Gh14solarApp: start update")
        var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					var json = JSON.parse( xmlhttp.responseText );
					powerW = 0;
					powerW += json.elecData.in.powerW;
					powerW -= json.elecData.out.powerW;
				} else {
		                    // Something went wrong, set counters to NaN
                			powerW = NaN;
					console.log("MyPowerApp: failed to collect p1 readings, server error code " + xmlhttp.status + ", message: " + xmlhttp.statusText)
                }
            }
        }
		xmlhttp.open("GET", serverUrl + serverP1Path, true);
		xmlhttp.send();
	}
	
	Timer {
        id: p1UpdateTimer
        interval: 5000  // update every 5 seconds
        triggeredOnStart: true
        running: true
        repeat: true
        onTriggered: update()
    }
}
