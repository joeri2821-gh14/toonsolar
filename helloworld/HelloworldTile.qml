import QtQuick 2.1
import qb.components 1.0

/*
 * Tile for showing helloworld info. Requires HelloworldApp.qml for data presentation.
 */

Tile {
    id: helloWorldTile
	property variant arrayColors : [colors.powerTileBar0,
									colors.powerTileBar1,
									colors.powerTileBar2,
									colors.powerTileBar3,
									colors.powerTileBar4,
									colors.powerTileBar5,
									colors.powerTileBar6,
									colors.powerTileBar7,
									colors.powerTileBar8,
									colors.powerTileBar9]
	
       property string value : "-"

	QtObject {
		id: p

		property int tileBars : 10

		function redraw() {
			var pb = powerList.children;
			//var avg = app.powerUsageData.avgValue;
                        var avg = 250;
			avg = avg === 0 ? 1 : avg;
			//value = app.powerUsageData.value;
                        value = 550;
			var filledBars;
			if (isNaN(value) || isNaN(avg)) {
				filledBars = 0;
				powerValue.text = '-';
			} else {
				filledBars = Math.round(value / (avg / 3));
				powerValue.text = qsTr("%1 Watt").arg(value);
			}

			for (var i = 0; i < tileBars; i++) {
				pb[i].color =  (i < filledBars) ? colors.graphGasDistrictHeat :
				colors.powerTileBarEmpty;
			}
		}
	}

	function init() {
		p.redraw();
	}


	onDimStateChanged: p.redraw()

	Text {
		id: powerWidgetText
		color: colors.tileTitleColor
		text: qsTr("Power at this moment")
		anchors {
			baseline: parent.top
			baselineOffset: Math.round(30 * verticalScaling)
			horizontalCenter: parent.horizontalCenter
		}
		font.pixelSize: qfont.tileTitle
		font.family: qfont.regular.name
	}

	PowerList {
		id: powerList
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.verticalCenter: parent.verticalCenter
	}

	Text {
		id: powerValue
		color: colors.tileTextColor
		anchors {
			horizontalCenter: parent.horizontalCenter
			baseline: parent.bottom
			baselineOffset: designElements.vMarginNeg16
		}
		font.pixelSize: qfont.tileText
		font.family: qfont.regular.name
	}  		
}
