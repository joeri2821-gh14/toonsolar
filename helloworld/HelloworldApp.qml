import QtQuick 2.1
import qb.components 1.0
import qb.base 1.0
import "helloWorld.js" as HelloWorldJS
import BxtClient 1.0

/*
 * HelloworldApp.qml
 *
 * Toon dummy application. Data is passed on to HelloworldTile.qml
 * 
 */

App {
    id: helloWorldApp
    
    // These are the URL's for the QML resources from which our widgets will be instantiated.
    // By making them a URL type property they will automatically be converted to full paths,
    // preventing problems when passing them around to code that comes from a different path.
    
    property url tileUrl : "HelloworldTile.qml"
    property url thumbnailIcon: "qrc:/tsc/boilerstatus.png"
    //property url thumbnailIcon: "./drawables/boilerstatus.png"
    
    property int unixTimestamp: HelloWorldJS.getUnixTime()
    property int counter: 0
    
    property variant connectedInfo: {'lowRateStartHour': 23, 'highRateStartHour': 8}
    property variant powerUsageData: ({})
   
    function init() {
                console.log("HelloWorldApp: init")
		registry.registerWidget( "tile", tileUrl, this, null, 
		 { thumbLabel: qsTr("Hello world"), 
		   thumbIcon: thumbnailIcon, 
		   thumbCategory: "general", 
		   thumbWeight: 30, 
		   baseTileWeight: 10, 
		   thumbIconVAlignment: "center" } );

	}

    function update() {
        console.log("HelloWorldApp: update") 
	counter = counter + 1;
    }   

    Timer {
		id: datetimeTimer
		interval: 10000  // update every 10 seconds
		triggeredOnStart: true
		running: true
		repeat: true
		onTriggered: update()
    }
    
    BxtDiscoveryHandler {
		id: pwrusageDiscoHandler
		deviceType: "happ_pwrusage"
		onDiscoReceived: {
			console.log("BxtDiscoveryHandler happ_pwrusage onDiscoReceived") 
            //p.pwrusageUuid = deviceUuid;
		}
	}
    
    BxtDiscoveryHandler {
		id: p1DiscoHandler
		deviceType: "hdrv_p1"
		onDiscoReceived: {
            console.log("BxtDiscoveryHandler hdrv_p1 onDiscoReceived") 
			//p.p1Uuid = deviceUuid;
		}
	}
    
    BxtDatasetHandler {
		id: powerUsageDataset
		dataset: "powerUsage"
		discoHandler: pwrusageDiscoHandler
		onDatasetUpdate: {
            console.log("BxtDatasetHandler powerUsage") 
            parseUsageDataset(update)
        }
	}
    
    BxtDatasetHandler {
		id: connectedInfoDataset
		dataset: "connectedInfo"
		discoHandler: p1DiscoHandler
		onDatasetUpdate: {
            console.log("BxtDatasetHandler connectedInfo")
            parseConnectedInfo(update);
        }
	}
    
    function parseConnectedInfo(msg) {
		var info = connectedInfo;

		var infoNode = msg.child;
		while (infoNode) {
			info[infoNode.name] = parseInt(infoNode.text);
            console.log("parseConnectedInfo node " + infoNode.name + " = " + info[infoNode.name]);
            infoNode = infoNode.sibling;
		}
		connectedInfo = info;
	}
    
    function parseUsageDataset(msg) {
        console.log("parseUsageDataset...");
		if (!msg)
			return;

        var tmpData = powerUsageData;

		var node = msg.child;
		while (node) {
			tmpData[node.name] = parseFloat(node.text);
			node = node.sibling;
		}
        
        powerUsageData = tmpData;
		console.log("parseUsageDataset done");
        
        console.log("powerUsageData.value:" + powerUsageData.value);
        console.log("powerUsageData.avgValue:" + powerUsageData.avgValue);
        console.log("powerUsageData.valueProduced:" + powerUsageData.valueProduced);
        console.log("powerUsageData.avgProduValue:" + powerUsageData.avgProduValue);
	}
    
}
